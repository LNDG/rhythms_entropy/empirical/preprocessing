function [mat_out, M, S] = mwb_zscore(mat_in,d, nanflag)

% computes a z-scored mat_out form mat_in, along the dimension d (default = 1)
% if nanflag = 1, nanmean/nanstd is used instead of mean/std (default: nanflag = 0)

% some defaults
if nargin < 2; d = 1; nanflag = 0; end
if nargin < 3; nanflag = 0;        end

sz = size(mat_in);
if length(sz) > 2; error('assuming 2 dimensional mat_in ...\n'); end

mat_in = double(mat_in);

if nanflag == 1    
    m = nanmean(mat_in,d);
    s = nanstd(mat_in,[],d);
else
    m = mean(mat_in,d);
    s = std(mat_in,[],d);
end

if d < 2
    M = repmat(m,sz(1),1);
    S = repmat(s,sz(1),1);
else
    M = repmat(m,[1,sz(2)]);
    S = repmat(s,[1,sz(2)]);
end

%output
mat_out = (mat_in - M)./S;


