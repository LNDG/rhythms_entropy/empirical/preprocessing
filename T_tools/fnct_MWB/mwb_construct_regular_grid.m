function grid = mwb_construct_regular_grid(cfg)

% cfg.dim
% cfg.bnd
% cfg.res


if ~isfield(cfg,'res'), cfg.res = 3; end
if ~isfield(cfg,'bnd'), error('no compartment defined'); end
if ~isfield(cfg,'dim'), error('no dim defined'); end
if ~isfield(cfg,'unit'), 
    fprintf('assuming cfg.unit = mm for all structures ..\n'); 
    cfg.unit = 'mm';
end

% determine extension of compartment
in.max = max(cfg.bnd.pnt,[],1);
in.min = min(cfg.bnd.pnt,[],1);
in.diff = in.max - in.min;

in.ext  = round((in.diff + cfg.res) ./ 2);

grid.xgrid = [-in.ext(1):cfg.res:in.ext(1)];
grid.ygrid = [-in.ext(2):cfg.res:in.ext(2)];
grid.zgrid = [-in.ext(3):cfg.res:in.ext(3)];

% refine output
% grid.resolution  = cfg.res;
grid.dim         = cfg.dim;
grid.sourceunits = cfg.unit;













