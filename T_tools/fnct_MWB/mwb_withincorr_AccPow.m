function outdat = mwb_withincorr_AccPow(cfg, indat, trlvec)

%--------------------------------------------------------------------------
%
% MWB_WITHINCORR_ACCPOW performs within subject point-biserial correlations
% between power and accuracy measures
%
% indat  = structure from ft_freqanalysis containing tfr-data
% trlvec = vector containing binary data reflecting the outcome variable in
% each trial
% cfg = structure with additional settings (can be empty, i.e., [])
%
% MWB, 10/2014
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% check defaults ...
%--------------------------------------------------------------------------
if ~isfield(cfg,'parameter'),     cfg.parameter = 'powspctrm'; end
if ~isfield(cfg,'bootstrapflag'), cfg.bootstrapflag = 1;       end % perform bootstrap
if ~isfield(cfg,'nrboot'),        cfg.nrboot = 200;            end % default nr of bootstraps
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% collect basic information ...
%--------------------------------------------------------------------------
nr.trl  = size(indat.(cfg.parameter),1);
nr.chan = size(indat.(cfg.parameter),2);
nr.freq = size(indat.(cfg.parameter),3);
nr.time = size(indat.(cfg.parameter),4);

% check consitency
if length(trlvec) ~= nr.trl, error('not the same number of trials in data and trlvec \n'); end
if (sum(trlvec == 0) + sum(trlvec == 1)) ~= nr.trl, error('trlvec must only contain 0 and 1 values \n'); end
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% compute point-biserial correlation ...
%--------------------------------------------------------------------------
trlvec = logical(trlvec);

% length of group 1
n1 = sum(trlvec);
% length of group 0
n0 = sum(~trlvec);

% compute means for each group
m1 = squeeze(mean(indat.(cfg.parameter)(trlvec,:,:,:),1));
m0 = squeeze(mean(indat.(cfg.parameter)(~trlvec,:,:,:),1));

% sd
sx = squeeze(std(indat.(cfg.parameter),[],1));

% correlation coefficient
r_orig = ((m1 - m0) ./ sx) .* sqrt((n1.*n0)./nr.trl.^2);
clear m1 m0 sx n1 n0;
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% compute boostraps
%--------------------------------------------------------------------------
if cfg.bootstrapflag
    
    % pre-allocate memory 
    r_boot = zeros(cfg.nrboot,nr.chan,nr.freq,nr.time);
    
    for i1 = 1:cfg.nrboot
        
        fprintf('running bootstrap %d from %d \n',i1,cfg.nrboot);
        
        rs = [];
        rs = randsample(nr.trl,nr.trl,true);
        
        bootvec = [];
        bootvec = trlvec(rs);
        
        % compute correlation
        % length of group 1
        n1 = sum(bootvec);
        % length of group 0
        n0 = sum(~bootvec);
        
        % compute means for each group
        m1 = squeeze(mean(indat.(cfg.parameter)(rs(bootvec),:,:,:),1));
        m0 = squeeze(mean(indat.(cfg.parameter)(rs(~bootvec),:,:,:),1));
        
        % sd
        sx = squeeze(std(indat.(cfg.parameter)(rs,:,:,:),[],1));
        
        % correlation coefficient
        r_boot(i1,:,:,:) = ((m1 - m0) ./ sx) .* sqrt((n1.*n0)./nr.trl.^2);
        clear m1 m0 sx n1 n0;
        
    end
    clear rs bootvec i1;
    
    % compute associated z-values via p-value
    r_z = zeros(size(r_orig));
    for i1 = 1:nr.chan
        for i2 = 1:nr.freq
            for i3 = 1:nr.time
                
                r_tmp = [];
                r_tmp = squeeze(r_orig(i1,i2,i3));
                
                if ~isnan(r_tmp)
                    
                    bootdis_tmp = [];
                    bootdis_tmp = squeeze(r_boot(:,i1,i2,i3));
                    
                    if r_tmp < 0
                        
                        p_tmp = [];
                        p_tmp = sum(bootdis_tmp >= 0) ./ cfg.nrboot;
                        
                        r_z(i1,i2,i3) = norminv(p_tmp);
                        
                    else
                        
                        p_tmp = [];
                        p_tmp = sum(bootdis_tmp <= 0) ./ cfg.nrboot;
                        
                        r_z(i1,i2,i3) = norminv(1-p_tmp);
                        
                    end
                    
                else
                    r_z(i1,i2,i3) = NaN;
                end
            end
        end
    end
    clear i1 i2 i3 r_boot p_tmp bootdis_tmp r_tmp
end
%--------------------------------------------------------------------------


%--------------------------------------------------------------------------
% finalize output
%--------------------------------------------------------------------------
outdat         = rmfield(indat,cfg.parameter); % pass information from original structure
outdat.dimord  = 'chan_freq_time';
outdat.pb_corr = r_orig; % raw correlation coefficients
outdat.zval    = r_z;    % z-values
%--------------------------------------------------------------------------







