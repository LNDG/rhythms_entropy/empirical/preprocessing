function elecOut = mwb_hm_adjustelecstruct(elecIn)


%--------------------------------------------------------------------------
% prepare output
%--------------------------------------------------------------------------
elecOut = elecIn;
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% rename the fiducials
%--------------------------------------------------------------------------
tmp.search = {'fidt9','fidt10','fidnz'}; 
tmp.replace = {'lpa','rpa','nas'};

for i1 = 1:length(tmp.search)
    if sum(strcmp(elecIn.label,tmp.search{i1})) > 0
        
        elecOut.label(strcmp(elecIn.label,tmp.search{i1})) = {tmp.replace{i1}}; % left peri-auricular
        
    end
end
clear tmp i1;
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% reduce to scalp electrodes and fiducials
%--------------------------------------------------------------------------
tmp.idx = zeros(size(elecIn.label));
for i1 = 1:length(tmp.idx)
    tmp.probe = elecIn.label{i1};
    if (length(tmp.probe) > 2) & strcmp(tmp.probe(1:3),'sfh')
        tmp.idx(i1) = 1;
    end
end
tmp.idx = logical(tmp.idx);

elecOut.label(tmp.idx) = [];
elecOut.chanpos = elecIn.chanpos(~tmp.idx,:);
elecOut.elecpos = elecIn.elecpos(~tmp.idx,:);

clear tmp i1;
%--------------------------------------------------------------------------
    

