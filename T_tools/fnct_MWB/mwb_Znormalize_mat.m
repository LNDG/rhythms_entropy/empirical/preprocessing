function outmat = mwb_Znormalize_mat(inmat,dim)

% MWB, 05/03/2014

if nargin < 2, dim = 1; end

s = size(inmat);

selvec = ones(size(s)); selvec(dim) = s(dim);

m_mat = mean(inmat,dim);
s_mat = std(inmat,[],dim);

M = repmat(m_mat,selvec); clear m_mat;
S = repmat(s_mat,selvec); clear s_mat;

outmat = (inmat - M) ./ S;

