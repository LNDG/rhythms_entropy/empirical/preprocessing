function MWB_artfdetec_plotdetecres(cfg,data,SDlim)

%--------------------------------------------------------------------------
% required arguments:
%--------------------------------------------------------------------------
% data is the output of an automatic artifact detecion routine
% i.e., MWB_artfdetec_XXX
%
%--------------------------------------------------------------------------
% optional arguments
%--------------------------------------------------------------------------
% cfg.visualtype = {'chan'}, or {'trial'}, or {'chan','trial'};
%
%--------------------------------------------------------------------------
% MWB, 06/02/2014
%--------------------------------------------------------------------------

if nargin < 3, SDlim = 3; end

figure;
for iP = 1:length(cfg.visualtype)
    pdat = data.(cfg.visualtype{iP}).zscore;

    xl = [0,(length(pdat)+ 1)];
    yl = [-max(abs(pdat))-1,max(abs(pdat))+ 1];
    if (max(abs(pdat)) < 3), yl = [-4 4]; end

    subplot(2,1,iP)
    plot([1:(xl(2)-1)],pdat,'ok', ...
        'markersize',6,'markerfacecolor','k','markeredgecolor','w');
    set(gca,'fontsize',10,'fontweight','bold','linewidth',3);
    xlim(xl); ylim(yl); grid on; box off; hold on;
    lh1 = line([xl],[SDlim,SDlim]); set(lh1,'color','r','linewidth',3,'linestyle','--');
    lh2 = line([xl],[-SDlim,-SDlim]); set(lh2,'color','r','linewidth',3,'linestyle','--');
    ylabel('zscore(mean(zscores))','fontsize',12,'fontweight','bold','linewidth',3);

    if strcmp(cfg.visualtype{iP},'chan')
        title('CHANNELBASED / ACROSS TRIAL','fontsize',16,'fontweight','bold');
        set(gca,'xtick',[1:(xl(2)-1)],'xticklabel',data.label);
        xlabel('channels','fontsize',12,'fontweight','bold','linewidth',3);
    else
        title('TRIALBASED / ACROSS CHAN','fontsize',16,'fontweight','bold');
        set(gca,'xtick',[1,round((xl(2)-1)./2),(xl(2)-1)],'xticklabel',[1,round((xl(2)-1)./2),(xl(2)-1)]);
        xlabel('trial number','fontsize',12,'fontweight','bold','linewidth',3);
    end
end