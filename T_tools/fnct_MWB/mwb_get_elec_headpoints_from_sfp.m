
function mwb_get_elec_headpoints_from_sfp(fname,id,outpath)

% fname = '/home/werkle/mount/MPIB07/EEG-Desktop2/MERLIN/D_data/3101/anatomy_mwb/3101_Merlin.sfp';

% read the sfp-file ...
fid = fopen(fname);
tmp = textscan(fid, ' %[^ \t]%n%n%n');
fclose(fid); clear fid;

sens.label   = tmp{1};
sens.elecpos = [tmp{2:4}];

% check the input
elec.label = {}; elec.pos = [];
head.label = {}; head.pos = [];

for i1 = 1:length(sens.label)
    
    if ~strcmp(sens.label{i1}(1:2),'sf');
        
        if strcmp(sens.label{i1},'fidnz') % nasion
            elec.label(end+1,1) = {'nas'};
            elec.pos(end+1,:)   = sens.elecpos(i1,:);
            
            head.label(end+1,1) = {'Hnas'};
            head.pos(end+1,:)   = sens.elecpos(i1,:);
            
        elseif strcmp(sens.label{i1},'fidt9') % lpa
            
            elec.label(end+1,1) = {'lpa'};
            elec.pos(end+1,:)   = sens.elecpos(i1,:);
            
            head.label(end+1,1) = {'Hlpa'};
            head.pos(end+1,:)   = sens.elecpos(i1,:);
            
        elseif strcmp(sens.label{i1},'fidt10') % rpa
            
            elec.label(end+1,1) = {'rpa'};
            elec.pos(end+1,:)   = sens.elecpos(i1,:);
            
            head.label(end+1,1) = {'Hrpa'};
            head.pos(end+1,:)   = sens.elecpos(i1,:);
            
        else
            
            elec.label(end+1,1) = {sens.label{i1}};
            elec.pos(end+1,:)   = sens.elecpos(i1,:);
            
            head.label(end+1,1) = {['H',sens.label{i1}]};
            head.pos(end+1,:)   = sens.elecpos(i1,:);
            
        end
    end
end
clear tmp sens i1;

% write elec and head files
if ~isstr(id), id = num2str(id); end

% elec
fidelecout = fopen([outpath,id,'_elec.sfp'],'w+');
for i1 = 1:length(elec.label)
    fprintf(fidelecout,'%s\t',elec.label{i1});
    fprintf(fidelecout,'%f\t%f\t%f\n',elec.pos(i1,:));
end
fclose(fidelecout);
    
% head-points
fidheadout = fopen([outpath,id,'_headpoints.sfp'],'w+');
for i1 = 1:length(head.label)
    fprintf(fidelecout,'%s\t',head.label{i1});
    fprintf(fidelecout,'%f\t%f\t%f\n',head.pos(i1,:));
end
fclose(fidheadout);


