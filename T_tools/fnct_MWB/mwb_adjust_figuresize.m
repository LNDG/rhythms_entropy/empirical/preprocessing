function fh = mwb_adjust_figuresize(fh,cfg)

if nargin < 2, cfg = []; end
if ~isfield(cfg,'width'),  cfg.width = 29.7; end
if ~isfield(cfg,'height'), cfg.height = 21;  end
if ~isfield(cfg,'orientation'), cfg.orientation = 'landscape';  end
if ~isfield(cfg,'size'), cfg.size = [cfg.width,cfg.height];  end

% Here we preserve the size of the image when we save it.
set(fh,'PaperUnits', 'centimeters');
set(fh,'PaperOrientation',cfg.orientation);
set(fh,'PaperSize',cfg.size);

papersize = get(fh, 'PaperSize');
left = (papersize(1)- cfg.width)/2;
bottom = (papersize(2)- cfg.height)/2;
myfiguresize = [left, bottom, cfg.width, cfg.height];
set(fh,'PaperPosition', myfiguresize);