function elec_new = mwb_select_channels_in_elec(elec,desiredchanlabels)

% assume elec is a strucutre with N channels and electrode positions
% desiredchanlabels is a cell-array concerning strings with channel names,
% i.e.from data.label
% then use as:
% elec_new = mwb_select_channels_in_elec(elec,data.label)

% set up the output structure
elec_new = elec;

chanposflag = 0; elecposflag = 0;
if isfield(elec_new,'chanpos'), chanposflag = 1;  elec_new.chanpos = []; end
if isfield(elec_new,'elecpos'), elecposflag = 1;  elec_new.elecpos = []; end
elec_new.label = {};

for i1 = 1:length(desiredchanlabels)
    
    idx = [];
    idx = find(strcmp(desiredchanlabels{i1},elec.label));
    
    elec_new.label(i1,1) = elec.label(idx);
    
    if chanposflag, elec_new.chanpos(i1,:) = elec.chanpos(idx,:); end
    if elecposflag, elec_new.elecpos(i1,:) = elec.chanpos(idx,:); end
    
end


