function [G H] = THG_initialize_CSD_20151009

%% path definitions
pn.mont  = ['//mpib07/EEG-Desktop2/ComMemEEGTools/CSDtoolbox/resource/']; 

%% add CSD toolbox path
addpath(genpath(['//mpib07/EEG-Desktop2/ComMemEEGTools/CSDtoolbox/']),1,'-frozen');

%% load data
load('//mpib07/EEG-Desktop2/ComMemEEGTools/fnct_THG/electrodelayouts/chanlocs_eeglab_MPIB_64_electrodes.mat');

% get electrode labels
for e = 1:66
    E{e,1} = chanlocs(e).labels;
end; clear e
E([4 7 10 38 48 66]) = []; clear chanlocs

% get montage
M = ExtractMontage([pn.mont '10-5-System_Mastoids_EGI129.csd'],E);

% check montage
MapMontage(M)

% get tranformation matrices
[G,H] = GetGH(M);

% clear workspace
clear E M pn
