function [out] = cm_read_and_merge_trigger_20140226(A,B)

%% read & merge trigger

ID    = 3101;
BLOCK = 'L'; % BLOCK = 'R1'

%% construct paths

% path: general
pn.gen = '//mpib07/EEG-Desktop2/MERLIN/D_data/';

% path: history (e.g., EEG marked segments)
pn.hst = [pn.gen '/' num2str(ID) '/history/'];

% path: raw eeg data
pn.raw = [pn.gen '/' num2str(ID) '/raw_eeg/'];

% path: processed eeg data
pn.eeg = [pn.gen '/' num2str(ID) '/eeg/'];

%% load behavioral time stamps

% load behavioral data
load(['M:\MERLIN\D_data\3101\raw_beh\Merlin_' num2str(ID) '.mat'])

% get column index & behavioral time stamps
if strcmp(BLOCK,'L')
    ind = find(strcmp(indData(1,:),'trig_encoding'));
    X   = cell2mat(indData(2:end,ind));
    display([num2str(length(X)) ' trials found']);
elseif strcmp(BLOCK,'R1')
    ind = find(strcmp(indData(1,:),'trig_recall1'));
    X   = cell2mat(indData(2:end,ind));
    display([num2str(length(X)) ' trials found']);
end

%% get trigger

% load config for eeg data file names
if exist([pn.hst num2str(ID) '_config_' BLOCK '.mat'],'file')
    load([pn.hst num2str(ID) '_config_' BLOCK],'config')
% alternatively generate file names directly
else
    file_data = dir([pn.raw '*R1*.eeg']);
    file_head = dir([pn.raw '*R1*.vhdr']);
    config.data_file = file_data.name;
    config.header_file = file_head.name;
    clear file_*
end

% cfg for trigger detection
cfg.datafile    = [pn.raw config.data_file];
cfg.headerfile  = [pn.raw config.header_file];
cfg.trigchan    = 'Trig';
cfg.direction   = -1;    % check for downward-going deflections
cfg.mindiff     = 0.002; % at least 2 ms between two successive 'trigger'
% cfg.maxdiff     = 8;     % between two 'trigger' should not be more than cfg.maxdiff seconds
cfg.visualcheck = 0;

% get trigger
T = cm_get_trigger_from_channel_20140205(cfg);

%% check trigger

% same size
if ~(length(X) == length(T))
    warning('Trigger missing!')
    check_size = 0;
else
    check_size = 1;
end

% cross-correlation
[c,lags] = xcorr(sortrows(X),T);
lag = lags(find(c==max(c)));
r = corr(sortrows(X(1+lag:length(T)+lag)),T);

%% combine trigger

% keep behavioral time stamps
C = X;
C = sortrows(C,1);

% add NaNs
C(:,2) = NaN;

% add trigger
C(1+lag:length(T)+lag,2) = T;

% check merging
r_ = corr(C(:,1),C(:,2),'rows','pairwise');

% display correlation
display(['test correlation = ' num2str(r)])
display(['correlation after merging = ' num2str(r_)])

% check correlation
if r < .999
    check_corr = 0;
    warning('Deficient match of time stamps and trigger!')
else
    check_corr = 1;
end

% write log file if inconsistencies occur
% if check_corr == 0 || check_size == 0
% [pn.hst 'log_' num2str(ID) '_' BLOCK  '_merge_trigger.txt']
% end

%% include trigger in summary table

% generate empty trigger cell array
trig = cell(size(indData,1),1);

% match trigger in table
for l = 2:size(indData,1)
    if ~isempty(indData{l,ind})
        get = find(C(:,1)==indData{l,ind});
        trig{l,1} = C(get,2);
    end
end; clear l

% header
trig{1,1} = ['trig_' BLOCK];

% include trigger in table
if strcmp(BLOCK,'L')
    indData(:,43) = trig;
elseif strcmp(BLOCK,'R1')
    indData(:,44) = trig;
elseif strcmp(BLOCK,'R2')
    indData(:,45) = trig;
end
    
    
    