function [alpha Pepisode] = THG_IAF_variance_simulation_20150607(cfg)
%
% alpha = THG_IAF_variance_simulation_20150607(cfg)
%
% generates an alpha episode with varying frequency per n_cycles cycles
%
% output: alpha = alpha episode
% input:  cfg   - .fsample = sampling frequency
%               - .falpha  = predefined 5 discrete alpha frequencies (e.g.,
%                            to match BOSC frequency resolution)
%               - 
%               - .n_cycles = number of cycles per discrete frequency step
%               - .n_steps  = number of steps
%               - .seed     = random number generator seed
%               
% Note: currently five discrete, approximately normally distributed frequencies

    
    %% fixed parameters
    
    % SD resolution
    sd_resolution = .5; % .5 = +/- 0.5 SDs
    % max SD
    sd_max        =  2; %  2 = +/- 2 SDs is maximum value
    % maximal step size
    max_step = 2;
    
    %% default parameters (if not specified otherwise)
    
    if ~isfield(cfg,'fsample')
        cfg.fsample = 1000; % Hz
    end
    if ~isfield(cfg,'falpha')
        cfg.falpha = 2.^[3.28:.02:3.36];
    end
    if ~isfield(cfg,'n_cycles')
        cfg.n_cycles = 3;
    end
    if ~isfield(cfg,'n_steps')
        cfg.n_steps  = 10;
    end
    
%%  set random generator seeds
    randn('seed',cfg.seed)

    %% alpha generation with varying frequency (+/- 0.2 Hz)
    
    check = 0;
    while check == 0
    
    % generate random frequency distribution
    x = randn(cfg.n_steps,1);
    x = floor(x + sd_resolution);
    x(x >  sd_max) =  sd_max;
    x(x < -sd_max) = -sd_max;
    
    d = diff(x);
    if max(abs(d)) <= max_step
        check = 1;
    end
    
    end
    
    Pepisode = [sum(x==-2) sum(x==-1) sum(x==0) sum(x==1) sum(x==2)] / cfg.n_steps;
    
    for c = 1:cfg.n_cycles
        x_(c:cfg.n_cycles:length(x)*cfg.n_cycles,1) = x;
    end; clear c
    x = x_; clear x_
    
    % construct alpha
    x = [x; NaN];
    alpha = [];
    cnt = 1;
    while cnt < length(x)
 
        % start index
        ind1 = cnt;
        
        % collect same frequencies
        check = 0;
        while check == 0
            if x(cnt) == x(cnt+1)
                cnt = cnt + 1;
            else
                check = 1;
            end
        end; clear check

        % length episode
        episode = ind1:cnt;
        
        % temporary alpha frequency
        tmp_frq = cfg.falpha(x(cnt)+3);
        
        % generate alpha cycles
        if ~exist('rest','var')
            time = 0 : 1/cfg.fsample : floor(cfg.fsample/tmp_frq*(episode(end)-episode(1)+1))/cfg.fsample;
            rest = 1/tmp_frq*(episode(end)-episode(1)+1) - floor(cfg.fsample/tmp_frq*(episode(end)-episode(1)+1))/cfg.fsample;
        else
            time = 1/cfg.fsample-rest : 1/cfg.fsample : floor(cfg.fsample/tmp_frq*(episode(end)-episode(1)+1)+rest*cfg.fsample)/cfg.fsample;
            rest = 1/tmp_frq*(episode(end)-episode(1)+1) - floor(cfg.fsample/tmp_frq*(episode(end)-episode(1)+1)+rest*cfg.fsample)/cfg.fsample;
        end
        alpha = [alpha sin(2*pi*time*tmp_frq)];
        
        % clear variables
        clear ind1 episode tmp_frq time
        
        % set counter
        cnt = cnt + 1;
        
    end
