function data = cm_preprocessing_filter(data,cfg)
%
% data = cm_preprocessing_filter(data,cfg);
%
% filters segmented data 
%
% data            = preprocessed fieldtrip data structure
%
% cfg.hpfilter    = 'yes' / 'no'
% cfg.hpfreq      = high-pass frequency cutoff [Hz]
% cfg.hpfiltord   = filter order (recommended = 4)
% cfg.hpfilttype  = 'but' (Butterworth filter design)
%
% cfg.lpfilter    = 'yes'
% cfg.lpfreq      = low-pass frequency cutoff [Hz]
% cfg.lpfiltord   = filter order (recommended = 4)
% cfg.lpfilttype  = 'but' (Butterworth filter design)
%
% NOTE: currently only the Butterworth filter is implemented

% 22.01.2014 THG
% - currently only the Butterworth filter is implemented


%%  high-pass filter

% filter
if strcmp(cfg.hpfilter,'yes')
    
    for j = 1:length(data.trial)
        
        if strcmp(cfg.hpfilttype,'but');
            
            data.trial{j} = cm_filt_but_hp(data.trial{j}',data.fsample,cfg.hpfreq,cfg.hpfiltord)';

        else
        
            warning('Currently only the Butterworth filter is implemented. No filter applied.')
            
        end
        
    end; clear j
    
end

% update cfg structure
data.cfg.hpfilter    = cfg.hpfilter;
data.cfg.hpfreq      = cfg.hpfreq;
data.cfg.hpfiltord   = cfg.hpfiltord;
data.cfg.hpfilttype  = cfg.hpfilttype;
    
%%  low-pass filter

if strcmp(cfg.lpfilter,'yes')
    
    for j = 1:length(data.trial)
        
        if strcmp(cfg.lpfilttype,'but');
            
            data.trial{j} = cm_filt_but_lp(data.trial{j}',data.fsample,cfg.lpfreq,cfg.lpfiltord)';

        else
        
            warning('Currently only the Butterworth filter is implemented. No filter applied.')
            
        end
        
    end; clear j
    
end

% update cfg structure
data.cfg.lpfilter    = cfg.lpfilter;
data.cfg.lpfreq      = cfg.lpfreq;
data.cfg.lpfiltord   = cfg.lpfiltord;
data.cfg.lpfilttype  = cfg.lpfilttype;

