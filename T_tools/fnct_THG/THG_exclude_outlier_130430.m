function [data n_ex] = exclude_outlier_130430(data,sd,varargin)
%
% exclude_outlier_130430
% input:    data = data vector [nx1] or [1xn]
%           sd   = standard deviation around mean as exclusion criterion
%           'recursive' = if specified recursive exclusion
% output:   data = data vector with outliers excluded
%           n_ex = number of excluded data points

%% check that data is a vector & transform to column vector
[n m] = size(data);
if min([n m]) > 1
    error('first input argument must be a vector');
end
if  n == 1
	data = data';
	n = m;
end; clear n m

%% get variable input
if nargin == 3
    rec = varargin{1};
end

%% exclude outliers
% temporary data
tmp = data;

% non-recursive exclusion
if nargin < 3
    
    sd_ = sd*std(tmp,1);
    mn_ = mean(tmp);
    ex1 = find(tmp > mn_ + sd_);
    ex2 = find(tmp < mn_ - sd_);
    tmp([ex1 ex2]) = [];
    clear sd_ mn_ ex1 ex2
    
% recursive exclusion
elseif nargin == 3 && strcmp(rec,'recursive')
    
    check = 0;
    while check == 0
        sd_ = sd*std(tmp,1);
        mn_ = mean(tmp);
        ex1 = find(tmp > mn_ + sd_);
        ex2 = find(tmp < mn_ - sd_);
        if isempty([ex1; ex2])
            check = 1;
        else
            tmp([ex1; ex2]) = [];
        end
        clear sd_ mn_ ex1 ex2
    end; clear check

end

% calculate number of excluded data points
n_ex = length(data) - length(tmp);
% hand over output data
data = tmp;
