function CWT = THG_wlt_cmor_2_1_150528(dat,win)

CWT = single(zeros(length(dat.freq),length(dat.time)));

for f = 1:length(dat.freq)
    
    tic
    display(['processing: ' num2str(dat.freq(f)) ' Hz ...'])

    % define window borders (win corresponds to SD of the gaussian)
    t_end = round((win*dat.srate*(1/dat.freq(f))))/dat.srate;
    
    % define new time vector for wavelet construction
    t_tmp = [-1*t_end:1/dat.srate:t_end];
    
    % construct wavelet
    psi   = dat.freq(f) * sub_cmor_2_1((t_tmp) * dat.freq(f));
    psize = length(psi);
    wsize = (psize-1)/2; 
    wgt   = sum(abs(psi))/2;

    % convolution
    y = conv(dat.data,psi);
    CWT(f,:)=y(ceil(length(psi)/2):length(y)-floor(length(psi)/2));
    
    clear t_end t_tmp psi psize wsize wgt
    toc
    
end;                                                                       clear f

end

%% subfunction: complex morlet wavelet construction
function psi = sub_cmor_2_1(t)

fb = 2;
fc = 1;

psi = ((pi*fb)^(-0.5))*exp(2*i*pi*fc*t).*exp(-t.^2/fb);

clear fb fc t

end

