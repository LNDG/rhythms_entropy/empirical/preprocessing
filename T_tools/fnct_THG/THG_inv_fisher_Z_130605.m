function r = THG_inv_fisher_Z_130605(Z)

r = (exp(2.*Z) - 1) ./ (exp(2.*Z) + 1);
