function THG_preprocessing_resting_MERLIN_20141217(pn_gen,pn_fun,ID)

% - pn_gen = general data (structure) path
% - pn_fun = general function path
% - ID     = ID [numerical]
%
% required functions:
% - cm_remove_edges_20140122
% - cm_automatic_IC_detection_20141217
% - cm_arbitrary_segmentation_fieldtrip_20140126
% - cm_elec2dataelec_20140226
% - cm_chanlocs2MPIB64_20140126

%%  generate pn_gens from general pn_gen

    % pn_gen: history (e.g., EEG marked segments)
    pn.hst = [pn_gen '/' num2str(ID) '/history/'];

    % pn_gen: raw EEG data 
    pn.dat = [pn_gen '/' num2str(ID) '/raw_eeg/'];

    % pn_gen: processed eeg data
    pn.eeg = [pn_gen '/' num2str(ID) '/eeg/'];

%%  load predifined segments of raw data

    % load config
    load([pn.hst num2str(ID) '_config_Rest'],'config')
        
    % define segment(s) to be read by fieldtrip
    cfg.trl = config.pnts_data_ica1;
      
    % include in config structure
    cfg.headerfile  = [pn.dat config.header_file];
    cfg.datafile    = [pn.dat config.data_file];

    % define reading & preprocessing parameters
    cfg.channel     = {'all'};
    cfg.continuous  = 'yes';
    cfg.demean      = 'yes';

    cfg.reref       = 'yes';
    cfg.refchannel  = {'A1','A2'};
    cfg.implicitref = 'A2';

    cfg.hpfilter    = 'yes';
    cfg.hpfreq      = 1;
    cfg.hpfiltord   = 4;
    cfg.hpfilttype  = 'but';

    cfg.lpfilter    = 'yes';
    cfg.lpfreq      = 125;
    cfg.lpfiltord   = 4;
    cfg.lpfilttype  = 'but';

    % get data
    data = ft_preprocessing(cfg);

    % clear cfg structure
    clear cfg

%%  remove data at edges

    % define number of data points to remove
    cfg.rm = [5000 5000];

    % remove data points at beginning and end of segments
    data = cm_remove_edges_20140122(data,cfg);
    data = ft_checkdata(data,'feedback','yes');

    % clear variables
    clear cfg

%%  resampling before ICA [1000 Hz]

    % define settings for resampling
    cfg.resamplefs = 1000;
    cfg.detrend    = 'no';
    cfg.feedback   = 'no';
    cfg.trials     = 'all';

    % resample
    data = ft_resampledata(cfg,data);

    % clear variables
    clear cfg
        
%%  ICA

    % date
    dt = date;

    % ica config
    cfg.method           = 'runica';
    cfg.channel          = {'all','-ECG','-A2'};
    cfg.trials           = 'all';
    cfg.numcomponent     = 'all';
    cfg.demean           = 'no';
    cfg.runica.extended  = 1;
    cfg.runica.logfile   = [pn.hst 'log_' num2str(ID) '_Rest_ICA1_' dt '.txt'];

    % run ICA
    icadat = ft_componentanalysis(cfg,data);

%%  automatic ICA labeling

    [iclabels] = cm_automatic_IC_detection_20141217(data,icadat);

%%  save data for ICA labeling

    % - prepare: segmentation

    % define settings
    cfg.length = 2;
    cfg.n      = 200;
    cfg.type   = 'beg';

    % arbitrary segmentation - segments a 2 sec
    % NOTE original segments will be overwritten
    data = cm_arbitrary_segmentation_fieldtrip_20140126(data,cfg);
    data = ft_checkdata(data,'feedback','yes');

    % - include ICA solution
    data.topo 	   = icadat.topo;
    data.unmixing  = icadat.unmixing;
    data.topolabel = icadat.topolabel;
    data.cfg       = icadat.cfg;

    % - include ICA solution in config
    config.ica1.date      = dt;
    config.ica1.topo 	  = icadat.topo;
    config.ica1.unmixing  = icadat.unmixing;
    config.ica1.topolabel = icadat.topolabel;
    config.ica1.cfg       = icadat.cfg;
    config.ica1.iclabels.auto = iclabels;

    % fieldtrip format electrode information
    load([pn_fun '\fnct_THG\electrodelayouts\realistic_1005.mat'])
    data.elec = cm_elec2dataelec_20140226(realistic_1005,data);

    % EEGLAB format electrode information
    load([pn_fun '\fnct_THG\electrodelayouts\chanlocs_eeglab_MPIB_64_electrodes.mat'])
    data = cm_chanlocs2MPIB64_20140126(data,chanlocs);

    % - include channel information in config
    config.elec     = data.elec;
    config.chanlocs = data.chanlocs;

    % keep ICA labels
    data.iclabels = iclabels;

    % save data
    save([pn.eeg num2str(ID) '_Rest_Rlm_Fhl_Ica'],'data')

    % save config
    config.preproc_version = '20141217';
    save([pn.hst num2str(ID) '_config_Rest'],'config')

    % clear variables
    clear chanlocs cfg config data dt icadat iclabels realistic_1005 pn*

