function THG_addpath_fieldtrip_20150930(pn,add)
%
% adds the fieldtrip toolbox (fieldtrip-20140121) to the current path
%
% pn  = pathname to the ConMemEEGTools folder
% add = flags whether toolbox is added to the current path (1, default), 
%       or whether toolbox is added to the default path (0)


% 22.01.2014 THG
%
% 01.10.2015 THG: link to more recent fieldtrip version
  
% default: add toolbox to current path
if nargin == 1
    add = 1;
end

% restore default path if required 
if add == 0
    restoredefaultpath; clear RESTORE*
end

% add functions and toolbox paths
addpath([pn '/fieldtrip-20150930/'],'-end');