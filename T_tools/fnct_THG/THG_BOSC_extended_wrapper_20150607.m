function [Pepisode,detected,B] = THG_BOSC_extended_wrapper_20150607(data,cfg)
%
% [Pepisode,detected,B] = THG_BOSC_standard_wrapper_20150603(data,cfg)
%
% wrapper for BOSC
%
% input: data = [n x 1] time series
%        cfg  = config structure specifying BOSC parameters:
%               - .F          = frequency vector
%               - .fsample    = sampling frequency
%               - .wavenumber = number of cycles in Morlet wavelet [default: 6]
%               - .padding    = time before and after the actual time series 
%                               of interest [in sec]
%               - .percentile = BOSC percentile threshold [default: .95]
%               - .numcycles  = number of cycles required to qualify as
%                               oscillation [default: 3]
%
%                  default settings as in Caplan et al. (2015)
%
%               extended settings:
%               - .tf_bias    = 'yes': correction of amplitude bias of Morlet wavelet, 
%                                      therefore also correcting frequency
%                                      bias
%                               'no':  no correction [default]
%               - .bg_estim   = 'recursive': recursive background estimation
%                                            after removal of detected oscillations
%                                            (settings as above)
%                               'robust':    background estimation using robust 
%                                            regression (with standard parameters)
%                               'standard':  standard BOSC background
%                                            estimation [default]
%               - .bosc       = 'abundance': removal of frequency leakage
%                               'standard' [default]
%               - .bosc_fres  = relative frequency resolution [default 1.5; i.e., for 
%                               10 Hz: unique peak between 10/1.5 and 10*1.5 Hz]
%
%
% output: Pepisode [vector]
%         detected [matrix]
%         B        [matrix]

% 07.06.2015 THG

%%  default settings
    
    % BOSC settings
    if ~isfield(cfg,'wavenumber')
        cfg.wavenumber = 6;
    end
    if ~isfield(cfg,'percentile')
        cfg.percentile = .95;
    end
    if ~isfield(cfg,'numcycles')
        cfg.numcycles = 3;
    end

    % extended settings
    if ~isfield(cfg,'tf_bias')
        cfg.tf_bias = 'no';
    end
    if ~isfield(cfg,'bg_estim')
        cfg.bg_estim = 'standard';
    end
    if ~isfield(cfg,'bosc')
        cfg.bosc = 'standard';
    end

%%  wavelet transform

    if strcmp(cfg.tf_bias,'no')
        B = BOSC_tf(data,cfg.F,cfg.fsample,cfg.wavenumber);
    elseif strcmp(cfg.tf_bias,'yes')
        B = BOSC_tf_with_bias_correction_20150607(data,cfg.F,cfg.fsample,cfg.wavenumber);
    end
    
%%  background power estimation

    % B matrix w/o padding
    B = B(:,cfg.padding*cfg.fsample+1:end-cfg.padding*cfg.fsample);
    
    % background power estimation
    if strcmp(cfg.bg_estim,'standard')
        [pv,mp] = BOSC_bgfit(cfg.F,B);
    elseif strcmp(cfg.bg_estim,'recursive')
        rec.F            = cfg.F;
        rec.fsample      = cfg.fsample;
        rec.wavenumber   = cfg.wavenumber;    
        rec.percentile   = cfg.percentile;  
        rec.numcycles    = cfg.numcycles;    
        [pv,mp] = BOSC_bgfit_recursive_20150530(rec,B);
    elseif strcmp(cfg.bg_estim,'robust')
        [pv,mp] = BOSC_bgfit_robust_20150606(cfg.F,B);
    end
    
%%  BOSC thresholds
    
    [pt,dt] = BOSC_thresholds(cfg.fsample,cfg.percentile,cfg.numcycles,cfg.F,mp);
    
%%  oscillation detection

    detected = zeros(size(B));
    for f = 1:length(cfg.F)
        detected(f,:) = BOSC_detect(B(f,:),pt(f),dt(f),cfg.fsample);
    end; clear f

    % abundance detection
    if strcmp(cfg.bosc,'abundance')
        abn.F    = cfg.F;
        abn.fres = cfg.bosc_fres;
        detected = THG_abundance_detect_20150607(B,detected,abn);
    end
    
%%  Pepisode
    Pepisode = mean(detected,2);
    
