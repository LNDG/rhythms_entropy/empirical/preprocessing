function [iaf, amp, n] = THG_collect_iaf_20150209(table,range)

    % get peaks in alpha range
    get_ = find(table(:,1)>=range(1) & table(:,1)<=range(2));
    
    % number of peaks
    n = length(get_);
    
    % one peak
    if n == 1
        
        iaf = table(get_,1);
        amp = table(get_,2);
        
    % no peak
    elseif n == 0
        
        iaf = NaN;
        amp = NaN;
        
    % more than one peak
    elseif n > 1
        
        table  = table(get_,:);
        get2 = find(table(:,2)==max(table(:,2)),1);
        iaf = table(get2,1);
        amp = table(get2,2);
        
    end
