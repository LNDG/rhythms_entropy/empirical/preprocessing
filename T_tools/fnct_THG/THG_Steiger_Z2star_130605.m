function [z p] = THG_Steiger_Z2star_130605(abcd_4x4,n)
%
% testing whether the correlations r(A,B) = r(C,D)
%
%       a       b       c       d
%  a    aa
%  b    AB      bb
%  c    ac      bc      cc
%  d    ad      bd      CD      dd

r_ab = abcd_4x4(2,1);
r_cd = abcd_4x4(4,3);

r_ac = abcd_4x4(3,1);
r_ad = abcd_4x4(4,1);
r_bc = abcd_4x4(3,2);
r_bd = abcd_4x4(4,2);

Zab = THG_fisher_Z_130619(r_ab);
Zcd = THG_fisher_Z_130619(r_cd);

r_ab_cd = (r_ab + r_cd)/2;

Za = 1/2 .* ( ...
        (r_ac - r_ab * r_bc) * (r_bd - r_bc * r_cd) ...
      + (r_ad - r_ac * r_cd) * (r_bc - r_ab * r_ac) ...  
      + (r_ac - r_ad * r_cd) * (r_bd - r_ab * r_ad) ...  
      + (r_ad - r_ab * r_bd) * (r_bc - r_bd * r_cd) );  

CV2 = Za ./ ((1 - r_ab_cd.^2).^2);

% calculate z-value
z = ( sqrt(n - 3) * (Zab - Zcd) ) / sqrt(2 - 2.*CV2);

% p-value
[h p ci zval] = ztest(z,0,1);