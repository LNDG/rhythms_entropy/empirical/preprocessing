function index = THG_find_maxima_20080805(data,smpl_f,freq,crit,check)

% find index for ith maximum then set data within crit% of the dominant
% frequeny around this maximum to zero; then find i+1th
% continue while there are numbers > 0 in temp (=data)
temp = data;
win = round(1/freq*smpl_f*crit);
i = 1;
while (sum(temp > 0) > 0)
    index(i) = find(temp == max(temp),1);
    if index(i)-win > 0
        temp(index(i)-win:index(i)+win) = 0;
    else
        temp(1:index(i)+win) = 0;
    end    
    i = i+1;
end
index = sort(index);

% exclude first and last index if the maximum (or R wave) is close (< 10% of
% the heart frequency) to the beginning or end of the data
if index(1) < round(1/freq*smpl_f*0.10); index(1) = []; end
if index(end) > length(data) - round(1/freq*smpl_f*0.10); index(end) = []; end

if exist('check','var')
if check == 1
% check equidistance of indices
d_dist = diff(index); range_dist = range(d_dist);
% display warning if difference in distance is larger than 10% of the heart
% frequency
if range(diff(index)) > round(1/freq*smpl_f*0.10);
    display('your rat may suffer from arrythmia! please check the data...')
    % visual check for indices of maxima 
    figure; plot(data); %title(name)
    check(1:length(data)) = NaN; for j = 1:length(index); check(index(j)) = max(data)*0.8; end 
    hold on; plot(check,'r*'); legend('data','extracted maxima','location','SouthEast')
end
end
end

% visual check for indices of maxima 
    % figure; plot(data); %title(name)
    % check(1:length(data)) = NaN; for j = 1:length(index); check(index(j)) = max(data)*0.8; end 
    % hold on; plot(check,'r*'); legend('data','extracted maxima','location','SouthEast')

clear temp win d_dist
