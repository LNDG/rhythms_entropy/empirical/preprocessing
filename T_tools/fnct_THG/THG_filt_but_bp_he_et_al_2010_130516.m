function dataA = THG_filt_but_bp_he_et_al_2010_130516(data,fsample,bpf,order,plot_)
%
% data      = [1xn] or [nx1] time series
% fsample   = sampling frequency
% bpf       = [lo_cutoff_freq hi_cutoff_freq] 
% order     = filter order
% plot_     = 'on' --> plots data vs. filtered data
%
% adapted from He et al. (2010). The Temporal Structures and Functional
% Significance of Scale-free Brain Activity. Neuron, 66, 353-369. 
% doi: http://dx.doi.org/10.1016/j.neuron.2010.04.020
%
% THG 16.05.2013
% needs fixing: so far only works for data with length being even (multiple 
% of 2); current fix: if data length is odd, add the last data point to the
% end and do the filter for the time series of lenght n+1; in the end
% remove the last data point from the filtered time series...

%%% check data orientation %%%
m = size(data);
if m(1) > 1
    data = data';
end
clear m

%%% check data length %%% - hopefully temporary fix! %%%
odd = 0;
if mod(length(data),2) == 1
    data(end+1) = data(end);
    odd = 1;
end
    
%%% initialize parameters %%%
nsample = length(data); 
delta   = 1/fsample;
freq    = fsample * [0:nsample/2-1]/nsample;

%%% set up butterworth filtering weights %%%
period  = nsample/fsample;
hzpbin  = 1/period;
flo = bpf(1);
fhi = bpf(2);
i = [1:nsample/2+1];
r_lo = ((i-1)*hzpbin/flo).^(2*order);
factor_lo = r_lo./(1 + r_lo);
r_hi = ((i-1)*hzpbin/fhi).^(2*order);
factor_hi = 1./(1 + r_hi);
for i = nsample/2+2:nsample
    factor_lo(i) = factor_lo(nsample-i+2);
    factor_hi(i) = factor_hi(nsample-i+2);
end; clear i

%%% filter data %%%
fftx = fft(data);
for i = 1:nsample
   fftx_filt(i) = fftx(i) * sqrt(factor_lo(1,i) * factor_hi(1,i));
end
dataA = ifft(fftx_filt);

%%% remove last data point if nsample was odd %%%
if odd == 1
    data(end)  = [];
    dataA(end) = [];
end

%%  --------------------- plotting - if requested --------------------- %%
%%% check if plot is requested %%%
if exist('plot_','var')
if strcmp(plot_,'on')
    
    %%% open figure window %%%
    figure

    %%% plot raw vs. filtered data %%%
    subplot(311)
    plot([0:(length(data)-1)]/fsample,data,'black');
    hold on; plot([0:(length(data)-1)]/fsample,dataA,'r');
    legend('raw data','filtered data')
    xlim([0 (length(data)-1)/fsample])

    %%% plot FFT filter response in Bode plot %%%
    subplot(3,2,[3 5])
    fftfilt_bp = factor_lo(1:nsample/2).*factor_hi(1:nsample/2);
    fftfilt_bp_DB = 10*log10(fftfilt_bp);
    fftfilt_lp_DB = 10*log10(factor_hi(1:nsample/2));
    fftfilt_hp_DB = 10*log10(factor_lo(1:nsample/2));
    plot(log10(freq),fftfilt_bp_DB,'black','LineWidth',2.0)
    hold on
    plot(log10(freq),fftfilt_lp_DB,'r','LineWidth',2.0)
    plot(log10(freq),fftfilt_hp_DB,'g','LineWidth',2.0)
    title('Filter response from FFT filter, Bode plot');
    axis([0 2.5 -100 0]);
    grid on
    legend('bpss','lpss','hpss','location','southeast');
    xlabel('Log10(frequency/Hz)');
    ylabel('Power/dB');

    %%% plot FFT filter response in linear plot %%%
    subplot(3,2,[4 6])
    plot(freq,fftfilt_bp,'black','LineWidth',2.0)
    hold on
    plot(freq,factor_hi(1:nsample/2),'r','LineWidth',2.0)
    plot(freq,factor_lo(1:nsample/2),'g','LineWidth',2.0)
    title('Filter response from FFT filter, Linear plot');
    axis([0 250 0 1]);
    grid on
    legend('bpss','lpss','hpss','location','east');
    xlabel('frequency/Hz');
    ylabel('Power');

end
end
