function THG_addpath_FAlpha(pn,add)
%
% adds the FAlpha toolbox to the current path
%
% pn  = pathname to the ConMemEEGTools folder
% add = flags whether folder is added to the current path (1, default), 
%       or whether folder is added to the default path (0)


% 06.05.2015 THG

% default: add toolbox to current path
if nargin == 1
    add = 1;
end

% restore default path if required 
if add == 0
    restoredefaultpath; clear RESTORE*
end

% add functions and toolbox paths
addpath([pn '/FAlpha/'],'-end');