function [mse n] = THG_mse_20150922(data,cfg) 
%
% [mse n] = THG_mse_20150530(data,cfg) 
%
% adapted code from Natasha Kovacevic for calculation of sample entropy
%
% data         = 1xN cell array with N 1xn data segments (can be of different length)
% cfg.m        = pattern length; commonly set to 2
% cfg.r        = similarity criterion; commonly ranges between .1 and .5
% cfg.scales   = scales for whom to calculate SE
% cfg.sd       = predefined SD as comparison criterion

% 06.10.12 THG
% - loop across trials taken out
%
% 08.10.12 THG
% - adapted num_cg_tpts for calculation of SE in case there were no matching patterns:
%   --> num_cg_tpts_ = sum(~isnan(y));
%
% 09.10.12 THG
% - change of input data
%
% 07.01.14 THG
% - change of data preparation
% - change indexing of data points for mse calculation
%
% 08.01.14 THG
% - addition of fixed SD criterion
%
% 30.05.15 THG
% - allows to choose SD criterion setting
%
% 22.09.15 THG
% - added output variable: n = number of compared patterns

% get parameters
m      = cfg.m;
r      = cfg.r;
scales = cfg.scales;

% initialize sample entropy  
mse = zeros(numel(scales),1);
  
% calculate similarity criterion
if isfield(cfg,'sd')
    r_new = r * cfg.sd;
else
    r_new = r * std(cell2mat(data),1);
end

%%  loop through scales
for s = 1:numel(scales)
    
    sc = scales(s);
    
%%  coarse-grain time series at this scale

    for d = 1:length(data)
        
        % number of coarse-grained time points
        num_cg_tpts = floor(length(data{d})/sc);
        
        % initialize temporary data vector
        tmp{d} = zeros(num_cg_tpts,1);
        
        % calculate coarse_grained time series
        for t = 1:num_cg_tpts
            tmp{d}(t) = mean(data{d}((t-1)*sc + [1:sc]));
        end; clear t
        
        % clear variables
        clear num_cg_tpts
    
    end
    
%%  "concatenate" data
     
    y = [];
    
    for d = 1:length(tmp)

        % check for sufficient length of segment
        if length(tmp{d}) > m
        
            % concatenate data
            y = [y; tmp{d}];

            % generate indices of points to match (1)
            if ~exist('pnts','var')
                pnts = [1 : (length(tmp{d}) - m)]';
            elseif d > 1
                pnts = [pnts; [1 : (length(tmp{d}) - m)]' + pnts(end) + 2];
            end
        
        end
        
    end
    
    if exist('pnts','var')
    
        % generate indices of points to match (2)
        for m_ = 2:(m+1)
            pnts(:,m_) = pnts(:,m_-1)+1;
        end

        % count number of compared patterns
        for d = 1:length(tmp)
            if length(tmp{d}) > m
                n_patt(d,1) = length(tmp{d}) - m;
            else
                n_patt(d,1) = 0;
            end
        end

        n(s,1) = sum(n_patt);

    %%  calculate sample entropy of coarse grained time series

        % number of data points
        n_pnts = size(pnts,1);

        % set cont to zeros
        cont = zeros(m+1,1);

        for i = 1:n_pnts

            for l = (i+1):n_pnts

                k = 1;
                while k <= m && abs(y(pnts(i,k)) - y(pnts(l,k))) <= r_new
                    cont(k) = cont(k) + 1;
                    k = k+1;
                end

                if k == (m+1) && abs(y(pnts(i,k)) - y(pnts(l,k))) <= r_new
                    cont(k) = cont(k)+1;
                end

            end

        end

    %%  calculate sample entropy
        if cont(m+1) == 0 || cont(m) == 0

            nlin_sc = size(pnts,1);
            mse(s) = -log(1/((nlin_sc)*(nlin_sc -1)));

        else

            mse(s) = -log(cont(m+1)/cont(m));

        end

        % clear variables
        clear cont n_patt pnts tmp y
    
    else
        
        mse(s) = NaN;
        n(s)   = 0;
        
        % clear variables
        clear cont n_patt pnts tmp y
        
    end
        
%%  end loop scales
end

  
