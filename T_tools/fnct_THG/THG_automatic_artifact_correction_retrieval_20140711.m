function THG_automatic_artifact_correction_retrieval_20140711(pn,ID,BLOCK,segment)

% pn = path structure requiring following fields: 
% .hst = history folder
% .beh = raw behavioral data folder
% .eeg = raw eeg data folder
% .out = processed behavioral data folder
%
% ID has to be numeric

%MCS: trigger timing according to stimulus onset! RETRIEVAL ATTEMPTS

% required sub-functions:
% - 

%% read config file
load([pn.hst num2str(ID) '_config_' BLOCK '.mat'])

%% read behavioral & trigger data

% read behavioral & trigger data
load([pn.beh 'Merlin_' num2str(ID) '.mat'])

% generate trial structure
if strcmp(BLOCK,'R1')
    ind(1) = find(strcmp(indData(1,:),'trig_recall1'));
    ind(2) = find(strcmp(indData(1,:),['trig_' BLOCK]));
elseif strcmp(BLOCK,'R2')
    ind(1) = find(strcmp(indData(1,:),'trig_recall2'));
    ind(2) = find(strcmp(indData(1,:),['trig_' BLOCK]));
elseif strcmp(BLOCK,'R3')
    ind(1) = find(strcmp(indData(1,:),'trig_recall3'));
    ind(2) = find(strcmp(indData(1,:),['trig_' BLOCK]));
end

%%  generate trl field

trl_ = cell2mat(indData(2:end,[ind]));
trl_ = sortrows(trl_,2);

for j = 1:size(trl_,1)
    trl(j,1) = trl_(j,2)+segment(1);
    trl(j,2) = trl_(j,2)+segment(2);
    trl(j,3) = segment(1);
end; clear j

% get number of trials
n_trl = size(trl,1);

%%  check missing trigger
% 
 ex = find(isnan(trl(:,1)));

% MWB, changed, 25.08.2014
% % currently, stop processing and send error message if trigger missing
% if ~isempty(ex)
%     error('trigger missing')
% end

% currently, stop processing and send error message if trigger missing
if ~isempty(ex)
    fprintf('++++++++++++++++++++++++++++++\n');
    fprintf('WARNING: trigger missing !!!!\n');
    fprintf('++++++++++++++++++++++++++++++\n');
    fprintf('\n\n');
    XcheckX = input('do you want to continue anyway? y/n \n','s');
    
    if strcmp(XcheckX,'y') | strcmp(XcheckX,'Y')
        
        trl(isnan(trl(:,1)),:) = [];
        trl_(isnan(trl_(:,2)),:) = [];
       %recalculate number of trials
        n_trl = size(trl,1);

    elseif strcmp(XcheckX,'n') | strcmp(XcheckX,'N')
        
        error('trigger missing - STOPPING NOW');
        
    else
        error('something really strange happened ...')
    end
end


%MCS 25.08.2014: check if enough data points for padding available
 trl(end,2)> config.pnts
 
 if trl(end,2)> config.pnts
    fprintf('++++++++++++++++++++++++++++++\n');
    fprintf('WARNING: padding at end will not work if not eliminating trials!!!\n');
    fprintf('++++++++++++++++++++++++++++++\n');
    fprintf('\n\n');
    XcheckX = input('do you want to continue anyway? y/n \n','s');
    
    if strcmp(XcheckX,'y') | strcmp(XcheckX,'Y')
        
        trl(end,:) = [];
        trl_(end,:) = [];
       %recalculate number of trials
        n_trl = size(trl,1);

    elseif strcmp(XcheckX,'n') | strcmp(XcheckX,'N')
        
        error('padding not possible - STOPPING NOW');
        
    else
        error('something really strange happened ...')
    end
end


%%  get file names of EEG data

cfg.headerfile = [pn.dat config.header_file];
cfg.datafile   = [pn.dat config.data_file];

%%  define data segments

cfg.trl = trl;

%%  get data

%% - read data if not yet existent

if ~exist([pn.eeg num2str(ID) '_' BLOCK '_Retrieval_Rlm_Fhl_Ica_Seg.mat'],'file')

% define settings for reading & preprocessing
cfg.channel     = {'all','-66','-67','-68','-69','-70','-71','-72','-Trig'};
cfg.continuous  = 'yes';

cfg.padding     = 10;

cfg.demean      = 'yes';

cfg.reref       = 'yes';
cfg.refchannel  = {'A1','A2'};
cfg.implicitref = 'A2';

cfg.hpfilter    = 'yes';
cfg.hpfreq      = 0.2;
cfg.hpfiltord   = 4;
cfg.hpfilttype  = 'but';

cfg.lpfilter    = 'yes';
cfg.lpfreq      = 100;
cfg.lpfiltord   = 4;
cfg.lpfilttype  = 'but';

% read & preprocess data
raw = ft_preprocessing(cfg);

% clear cfg
clear cfg

%% - resample

% define settings for resampling
cfg.resamplefs = 250;
cfg.detrend    = 'no';
cfg.feedback   = 'no';
cfg.trials     = 'all';

% resample
raw = ft_resampledata(cfg,raw);

% clear variables
clear cfg

%%  - save raw data

save([pn.eeg num2str(ID) '_' BLOCK '_Retrieval_Rlm_Fhl_Ica_Seg'],'raw')

else
    
%% - alternatively: load data

load([pn.eeg num2str(ID) '_' BLOCK '_Retrieval_Rlm_Fhl_Ica_Seg'],'raw')

end

%% ICA (from weights)

% ica config
cfg.method           = 'runica';
cfg.channel          = {'all','-ECG','-A2'};
cfg.trials           = 'all';
cfg.numcomponent     = 'all';
cfg.demean           = 'yes';
cfg.runica.extended  = 1;

% ICA solution
cfg.unmixing     = config.ica1.unmixing;
cfg.topolabel    = config.ica1.topolabel;

% components
comp = ft_componentanalysis(cfg,raw);

% clear cfg
clear cfg raw

%% remove components

% get IC labels
iclabels = config.ica1.iclabels.manual;

% cfg for rejecting components (reject: blinks, eye movements, ecg, ref)
cfg.component = sortrows([iclabels.bli; iclabels.mov; iclabels.hrt; iclabels.ref])';
cfg.demean    = 'yes';

% reject components
data = ft_rejectcomponent(cfg,comp);

% clear cfg
clear cfg comp

%% remove eye & reference channels

cfg.channel     = {'all','-IOL','-LHEOG','-RHEOG','-A1'};
cfg.demean      = 'yes';

% remove channels
data = ft_preprocessing(cfg,data);

% clear cfg
clear cfg

%% ------------------------- ARTIFACT DETECTION ------------------------ %%

% open log file
fid = fopen([pn.hst 'log_' num2str(ID) '_ArtCorr_Retrieval.txt'],'a');
    
% write log
fprintf(fid,['*********************  ' BLOCK '  *********************\n']);
fprintf(fid,['function: THG_automatic_artifact_correction_retrieval_20140711.m \n\n']);
fprintf(fid,['beh file = Merlin_' num2str(ID) '.mat\n']);
fprintf(fid,['eeg file = ' config.data_file '\n\n']);


%% get artifact contaminated channels by kurtosis, low & high frequency artifacts

cfg.criterion = 3;
cfg.recursive = 'no';

[index0 parm0 zval0] = THG_MWB_channel_x_epoch_artifacts_20140311(cfg,data);

% write log
tmp_log = '';
for j = 1:length(index0.c)
    tmp_log = [tmp_log num2str(index0.c(j)) ' '];
end; clear j
tmp_log = [tmp_log(1:end-1) '\n'];
fprintf(fid,'(1) automatic bad channel detection:\n');
fprintf(fid,['MWB:    channel(s) ' tmp_log]);

% clear cfg
clear cfg tmp_log

%% get artifact contaminated channels by FASTER

cfg.criterion = 3;
cfg.recursive = 'no';

[index1 parm1 zval1] = THG_FASTER_1_channel_artifacts_20140302(cfg,data);

% write log
tmp_log = '';
for j = 1:length(index1)
    tmp_log = [tmp_log num2str(index1(j)) ' '];
end; clear j
tmp_log = [tmp_log(1:end-1) '\n'];
fprintf(fid,['FASTER: channel(s) ' tmp_log]);

% clear cfg
clear cfg tmp_log

%% interpolate artifact contaminated channels

% collect bad channels
badchan = unique([index0.c; index1]);

fprintf(fid,['--> ' num2str(length(badchan)) ' channels interpolated\n\n']);

cfg.method     = 'spline';
cfg.badchannel = data.label(badchan);
cfg.trials     = 'all';
cfg.lambda     = 1e-5; 
cfg.order      = 4; 
cfg.elec       = config.elec;

% interpolate
data = ft_channelrepair(cfg,data);

% clear cfg
clear cfg

%% get artifact contaminated epoch by kurtosis, low & high frequency artifacts

cfg.criterion = 3;
cfg.recursive = 'no';

[index0 parm0 zval0] = THG_MWB_channel_x_epoch_artifacts_20140311(cfg,data);

% write log
fprintf(fid,'(2) automatic bad epoch detection:\n');
fprintf(fid,['MWB:    ' num2str(length(index0.t)) ' bad epoch(s) detected\n']);

% clear cfg
clear cfg

%% get artifact contaminated epochs by FASTER

% get epoch articfacts
cfg.criterion = 3;
cfg.recursive = 'yes';

[index2 parm2 zval2] = THG_FASTER_2_epoch_artifacts_20140302(cfg,data);

% write log
fprintf(fid,['FASTER: '  num2str(length(index2)) ' bad epoch(s) detected\n']);

% clear cfg
clear cfg

%% remove trials

% collect bad trials
badtrl = unique([index0.t; index2]);

% write log
fprintf(fid,['--> ' num2str(length(badtrl)) ' trials removed\n\n']);

trials = 1:length(data.trial);
trials(badtrl) = [];

cfg.trials      = trials;
cfg.demean      = 'yes';

% remove trials
data = ft_preprocessing(cfg,data);

% clear cfg
clear cfg

%% get channel x epoch artifacts

% cfg
cfg.criterion = 3;
cfg.recursive = 'no';

[index3 parm3 zval3] = THG_FASTER_4_channel_x_epoch_artifacts_20140302(cfg,data);

% write log
fprintf(fid,'(3) automatic single epoch/channel detection:\n');
fprintf(fid,['FASTER: ' num2str(length(index3)) ' channel(s)/trial(s) detected & interpolated\n\n']);

% clear cfg
clear cfg

%% interpolate channel x epoch artifacts

% interpolattion settings
cfg.method     = 'spline';
cfg.badchannel = [];
cfg.trials     = 'all';
cfg.lambda     = 1e-5; 
cfg.order      = 4; 
cfg.elec       = config.elec;

for t = 1:length(data.trial)
    
    % display
    display(['---------------- * ---------------- '])
    display(['correcting trial ' num2str(t)])
    
    % get trial
    rm.trials      = t;
    rm.demean      = 'yes';
    tmp = ft_preprocessing(rm,data);

    % define bad channels x epochs
    cfg.badchannel = data.label(index3(:,t)==1);

    % interpolate
    tmp = ft_channelrepair(cfg,tmp);
    
    % reset cfg
    cfg.badchannel = [];
    
    % get corrected channels x epochs
    data.trial{t} = tmp.trial{1};
    
    % clear variables
    clear tmp
    
end; clear t

% clear cfg
clear cfg

%% re-check channel x epoch artifacts

% cfg
cfg.criterion = 3;
cfg.recursive = 'yes';

[index4] = THG_FASTER_4_channel_x_epoch_artifacts_20140302(cfg,data);

% outlier detection: number of artifact contaminated channels per trial
tmp = sum(index4)';
tmp = cm_outlier2nan_20140311(tmp,'>',3,'yes');

index4 = find(isnan(tmp));

% write log
fprintf(fid,'(4) additional bad epoch detection:\n');
fprintf(fid,['FASTER: ' num2str(length(index4)) ' trials additionally excluded\n\n']);

% clear cfg
clear cfg tmp

%% remove additional trials

trials = 1:length(data.trial);
trials(index4) = [];

cfg.trials      = trials;
cfg.demean      = 'yes';

% remove trials
data = ft_preprocessing(cfg,data);

% clear cfg
clear cfg

%% finalize log

fprintf(fid,['final data set contains ' num2str(length(data.trial)) ' trials\n']);
fprintf(fid,['final data set: ' num2str(ID) '_' BLOCK '_Retrieval_Rlm_Fhl_Ica_Seg_Art.mat\n\n']);
fclose(fid);

%% save data
save([pn.eeg num2str(ID) '_' BLOCK '_Retrieval_Rlm_Fhl_Ica_Seg_Art'],'data')

%% collect artifacts

% bad channels
ArtDect.channels = badchan;

% bad trials (1)
ArtDect.trials1  = badtrl;

% bad trials (2)
tmp = [1:n_trl];
tmp(badtrl) = [];
ArtDect.trials2  = tmp(index4);
clear tmp

% bad single epoch(s)/channel(s)
ind = [1:n_trl];
ind(badtrl) = [];
tmp = ones(length(data.label),n_trl);
tmp(:,ind) = index3;
ArtDect.single = tmp;

% overall labeling
ArtDect.overall = ArtDect.single;
ArtDect.overall(ArtDect.channels,:) = 1;
ArtDect.overall(:,ArtDect.trials1) = 1;
ArtDect.overall(:,ArtDect.trials2) = 1;

% original trial numbers & triggers in final file
tmp = [1:n_trl]';
tmp = [tmp sortrows(trl_,2)];
tmp(badtrl,:) = [];
tmp(index4,:) = [];
ArtDect.final_file = [pn.eeg num2str(ID) '_' BLOCK '_Retrieval_Rlm_Fhl_Ica_Seg_Art.mat'];
ArtDect.final2original = tmp;

% save version
ArtDect.version = '20140311';

% add to config
config.ArtDect = ArtDect;

% save config
save([pn.hst num2str(ID) '_Retrieval_config_' BLOCK '.mat'],'config')

