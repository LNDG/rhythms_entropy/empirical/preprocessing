function mse = THG_mse_20140107(data,m,r,scales) 
%
% adapted code from Natasha Kovacevic for calculation of sample entropy
%
% data     = 1xN cell array with N 1xn data segments (can be of different length)
% m        = pattern length; commonly set to 2
% r        = similarity criterion; commonly ranges between .1 and .5
% scales   = scales for whom to calculate SE

% 06.10.12 THG
% - loop across trials taken out
%
% 08.10.12 THG
% - adapted num_cg_tpts for calculation of SE in case there were no matching patterns:
%   --> num_cg_tpts_ = sum(~isnan(y));
%
% 09.10.12 THG
% - change of input data
%
% 07.01.14 THG
% - change of data preparation
% - change indexing of data points for mse calculation

% initialize sample entropy  
mse = zeros(numel(scales),1);
  
% generate temporary data vector for SD calculation
tmp = [];
for j = 1:length(data)
    tmp = [tmp data{j}];
end

% calculate similarity criterion
sd    = std(tmp,1); clear tmp
r_new = r * sd;

%%  loop through scales
for s = 1:numel(scales)
    
    sc = scales(s);
    
%%  coarse-grain time series at this scale

    for d = 1:length(data)
        
        % number of coarse-grained time points
        num_cg_tpts = floor(length(data{d})/sc);
        
        % initialize temporary data vector
        tmp{d} = zeros(num_cg_tpts,1);
        
        % calculate coarse_grained time series
        for t = 1:num_cg_tpts
            tmp{d}(t) = mean(data{d}((t-1)*sc + [1:sc]));
        end; clear t
        
        % clear variables
        clear num_cg_tpts
    
    end
    
%%  concatenate data
     
    y = [];
    
    for d = 1:length(tmp)

        % concatenate data
        y = [y; tmp{d}];
        
        % generate indices of points to match (1)
        if d == 1
            
            pnts = [1 : (length(tmp{d}) - m)]';
            
        elseif d > 1
            
            pnts = [pnts; [1 : (length(tmp{d}) - m)]' + pnts(end) + 2];
        
        end
        
    end
    
    % generate indices of points to match (2)
    for m_ = 2:(m+1)
        
        pnts(:,m_) = pnts(:,m_-1)+1;
        
    end
        
    
%%  calculate sample entropy of coarse grained time series
      
    % number of data points
    n_pnts = size(pnts,1);
    
    % set cont to zeros
    cont = zeros(m+1,1);
    
    for i = 1:n_pnts
    
        for l = (i+1):n_pnts
            
            k = 1;
            while k <= m && abs(y(pnts(i,k)) - y(pnts(l,k))) <= r_new
                
                cont(k) = cont(k) + 1;
                k = k+1;
                
            end

            if k == (m+1) && abs(y(pnts(i,k)) - y(pnts(l,k))) <= r_new
                
                cont(k) = cont(k)+1;

            end
            
        end
        
    end

%%  calculate sample entropy
    if cont(m+1) == 0 || cont(m) == 0
        
        nlin_sc = size(pnts,1);
        mse(s) = -log(1/((nlin_sc)*(nlin_sc -1)));
        
    else
        
        mse(s) = -log(cont(m+1)/cont(m));
        
    end
    
%%  end loop scales
end

  
