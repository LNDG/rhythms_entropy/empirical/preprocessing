function THG_eegplot_20141216(EEG)

% adapted from pop_eegplot()
% visually inspect EEG data using a scrolling display

% define command
command = '[EEGTMP LASTCOM] = eeg_eegrej(EEG,eegplot2event(TMPREJ,-1));';

% define plot options
eegplotoptions = { 'events', EEG.event };
if ~isempty(EEG.chanlocs)
    eegplotoptions = { eegplotoptions{:}  'eloc_file', EEG.chanlocs };
end;

% plot
eegplot(EEG.data,'srate',EEG.srate,'title','Scroll channel activities -- eegplot()', ...
                 'limits',[EEG.xmin EEG.xmax]*1000,'command',command,eegplotoptions{:}); 

return;
