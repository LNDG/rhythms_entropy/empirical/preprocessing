function mrk = read_bvr_vmrk(filename)

A = textread(filename,'%s','delimiter','\n');

cnt = 1;
for j = 1:length(A)
    
    if isempty(A{j,1}) == 0
    if strcmp(A{j,1}(1:2),'Mk')
        
        %get temporary string containing marker info
        tmp = A{j,1};
        % get indices of "delimiter"
        ind1 = regexp(tmp, 'Mk');
        ind2 = regexp(tmp, '=');
        ind3 = regexp(tmp, ',');
        ind  = [ind1 ind2 ind3];

        % get marker
        mrk{cnt,1} = str2num(tmp(ind(1)+2:ind(2)-1));
        mrk{cnt,2} = tmp(ind(2)+1:ind(3)-1);
        mrk{cnt,3} = tmp(ind(3)+1:ind(4)-1);
        for k = 4:5
            mrk{cnt,k} = str2num(tmp(ind(k)+1:ind(k+1)-1));
        end; clear k
        
        clear tmp ind*
        
        if mrk{cnt,1} == cnt
            cnt = cnt + 1;
        else
            error('marker missing ???')
        end
        
    end
    end
    
end; clear j cnt
        