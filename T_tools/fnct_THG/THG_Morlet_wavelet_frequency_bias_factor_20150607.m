function CF_freq = THG_Morlet_wavelet_frequency_bias_factor_20150607(cfg)
%
% input:    cfg
%                   - .F          = required frequency resolution
%                   - .fsample    = sampling frequency
%                   - .wavelet    = specific wavelet implementation
%                   - .wavenumber = number of cycles
%                   - .seglength  = length of intended segment in sec
%
% output:   CF_freq = frequency correction factor

% THG 07.06.2015

%% frequency bias estimated from 10 Hz sine wave (8 sec)

    % frequency range
    F = [7:.001:10.1];
    
    % signal: 10 Hz
    time   = [0:1/cfg.fsample:cfg.seglength+(10+cfg.wavenumber/10)*2];
    signal = sin(time*10*2*pi); 

    % wavelet transform
    if strcmp(cfg.wavelet,'BOSC_tf');
        B = BOSC_tf(signal,F,cfg.fsample,cfg.wavenumber);
    end

    % peak
    pad1 = ceil((10+1/cfg.fsample)*cfg.fsample + cfg.wavenumber/10*cfg.fsample);
    pad2 = floor(20*cfg.fsample + cfg.wavenumber/10*cfg.fsample);
    peak = F(mean(B(:,pad1:pad2)')==max(mean(B(:,pad1:pad2)')));

    % frequency correction factor
    CF_freq = peak/10;

    % clear variables
    clear F time signal B pad* peak
 