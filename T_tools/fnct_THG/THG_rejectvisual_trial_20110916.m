function [TrlInfoArt ArtTab] = THG_rejectvisual_trial_20110916(cfg, data)
%
% new function; only trialwise presentations; THG 16.09.11

% define artefact labels
ArtLabels = {'good','blink','move','tongue','muscle','else'};

% determine the initial selection of trials and channels
nchan       = size(data.trial{1},1);
ntrl        = length(data.trial);
cfg.channel = {'all'}; %channelselection(cfg.channel, data.label);

% if trials were previously labeled, previous labeling will be included in 
% the current labeling session
% check whether field 'TrlInfoArt' exists; if not create it 
if isfield(data,'TrlInfoArt')
    for i = 1:ntrl
        tmp_ = data.TrlInfoArt{i,1};
        tmp  = zeros(1,length(ArtLabels));
        for j = 1:length(tmp_)
            tmp = strcmp(ArtLabels,{tmp_{1,j}}) + tmp;
        end; clear j tmp_
        ArtTab(i,:)     = tmp; clear tmp
    end; clear i
    ArtComment = data.TrlInfoArt(:,2);
    clear TrlInfoArt
elseif ~isfield(data,'TrlInfoArt')
    ArtTab     = zeros(ntrl,length(ArtLabels));
    ArtComment = cell(ntrl,1);
end

% compute the offset from the time axes
offset = zeros(ntrl,1);
for i=1:ntrl
    offset(i) = time2offset_fieldtrip(data.time{i}, data.fsample);
end
% determine the duration of each trial
for i=1:length(data.time)
    begsamplatency(i) = min(data.time{i});
    endsamplatency(i) = max(data.time{i});
end
% determine the latency window which is possible in all trials
minperlength = [max(begsamplatency) min(endsamplatency)];
maxperlength = [min(begsamplatency) max(endsamplatency)];
% latency window for averaging and variance computation is given in seconds
cfg.latency = [];
cfg.latency(1) = maxperlength(1);
cfg.latency(2) = maxperlength(2);
% select the specified latency window from the data
% this is done AFTER the filtering to prevent edge artifacts
for i=1:ntrl
  begsample = nearest_fieldtrip(data.time{i}, cfg.latency(1));
  endsample = nearest_fieldtrip(data.time{i}, cfg.latency(2));
  data.time{i} = data.time{i}(begsample:endsample);
  data.trial{i} = data.trial{i}(:,begsample:endsample);
end; clear i begsamp* endsamp* maxperl* minperl*

% open figure
h = figure('position',[100 100 512 768]);
axis([0 1 0 1.005])
axis off

% the info structure will be attached to the figure 
% and passed around between the callback functions 
info             = [];
info.ncols       = 1;
info.nrows       = nchan;       
info.trlop       = 1;
info.quit        = 0;
info.ntrl        = ntrl;        clear ntrl
info.nchan       = nchan;       clear nchan
info.data        = data;        clear data
info.cfg         = cfg;         clear cfg
info.offset      = offset;      clear offset
info.ArtLabels   = ArtLabels;   
info.ArtTab      = ArtTab;      clear ArtTab
info.ArtComment  = ArtComment;  clear ArtComment
info.showlabel   = 'off';
info.fact        = 2;
% determine the position of each subplot within the axis
for row=1:info.nrows
  for col=1:info.ncols
    indx = (row-1)*info.ncols + col;
    if indx>info.nchan
      continue
    end
    info.x(indx)     = (col-0.9)/info.ncols;
    info.y(indx)     = 1 - (row-0.45)/(info.nrows+1);
    clear indx
  end; clear col
end; clear row
info.label = info.data.label(1:info.nchan,1);

guidata(h,info);

uicontrol(h,'units','pixels','position',[  5  4 50 84],'String','quit','ForegroundColor','w','BackgroundColor','k','Callback',@stop);
uicontrol(h,'units','pixels','position',[ 67 48 27 40],'String','<', 'BackgroundColor','w','Callback',@prev);
uicontrol(h,'units','pixels','position',[ 94 48 27 40],'String','>', 'BackgroundColor','w','Callback',@next);
uicontrol(h,'units','pixels','position',[126 48 27 40],'String','<<','BackgroundColor','w','Callback',@prev10);
uicontrol(h,'units','pixels','position',[153 48 27 40],'String','>>','BackgroundColor','w','Callback',@next10);

% textfield for trial #
uicontrol(h,'units','normalized','position',[.45 .97 .10 .025],'Style','Edit','String',num2str(info.trlop),'Callback',@edittext);

uicontrol(h,'units','pixels','position',[190 26 50 31],'String','good>',  'BackgroundColor','w','Callback',@markgood_next);
uicontrol(h,'units','pixels','position',[242 26 50 31],'String','blink>', 'BackgroundColor','w','ForegroundColor','r','Callback',@markblink_next);
uicontrol(h,'units','pixels','position',[294 26 50 31],'String','move>',  'BackgroundColor','w','ForegroundColor','g','Callback',@markmove_next);
uicontrol(h,'units','pixels','position',[346 26 50 31],'String','tongue>','BackgroundColor','w','ForegroundColor','c','Callback',@marktongue_next);
uicontrol(h,'units','pixels','position',[398 26 50 31],'String','muscle>','BackgroundColor','r','Callback',@markmuscle_next);
uicontrol(h,'units','pixels','position',[448 26 50 31],'String','else>',  'BackgroundColor','r','Callback',@markelse_next);

uicontrol(h,'units','pixels','position',[190 57 50 31],'String','good',  'BackgroundColor','w','Callback',@markgood_same);
uicontrol(h,'units','pixels','position',[242 57 50 31],'String','blink', 'BackgroundColor','w','ForegroundColor','r','Callback',@markblink_same);
uicontrol(h,'units','pixels','position',[294 57 50 31],'String','move',  'BackgroundColor','w','ForegroundColor','g','Callback',@markmove_same);
uicontrol(h,'units','pixels','position',[346 57 50 31],'String','tongue','BackgroundColor','w','ForegroundColor','c','Callback',@marktongue_same);
uicontrol(h,'units','pixels','position',[398 57 50 31],'String','muscle','BackgroundColor','r','Callback',@markmuscle_same);
uicontrol(h,'units','pixels','position',[448 57 50 31],'String','else',  'BackgroundColor','r','Callback',@markelse_same);

uicontrol(h,'units','pixels','position',[67 26 113 20],'String','labels on','BackgroundColor','w','Callback',@labels);

uicontrol(h,'units','pixels','position',[110 4 70 15],'Style','Text','String','comment:')
edit1 = uicontrol(h,'units','pixels','position',[190  3 308 18],'Style','edit','Callback',@comment_callback);

% core loop
interactive = 1;
while interactive && ishandle(h)
    redraw(h);
    info = guidata(h);
    set(edit1,'String',info.ArtComment{info.trlop,1})
    % textfield for trial #
    uicontrol(h,'units','normalized','position',[.45 .97 .10 .025],'Style','Edit','String',num2str(info.trlop),'Callback',@edittext);
    % label on/off
    if strcmp(info.showlabel,'on')
        onoff = 'off';
    elseif strcmp(info.showlabel,'off')
        onoff = 'on';
    end
    uicontrol(h,'units','pixels','position',[67 26 113 20],'String',['labels ' onoff],'BackgroundColor','w','Callback',@labels);
    clear ononff
    if info.quit == 0,
        uiwait;
    else
        ArtTab = info.ArtTab;
        for j = 1:size(ArtTab,1)
            TrlInfoArt{j,1} = ArtLabels(1,find(ArtTab(j,:)==1));
            TrlInfoArt(j,2) = info.ArtComment(j,1);
        end
        delete(h);
        break
    end
end % while interactive


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function varargout = prev(h, eventdata, handles, varargin)
info = guidata(h);
if info.trlop > 1,
  info.trlop = info.trlop - 1;
end;
guidata(h,info);
uiresume;

function varargout = next(h, eventdata, handles, varargin)
info = guidata(h);
if info.trlop < info.ntrl,
  info.trlop = info.trlop + 1;
end;
guidata(h,info);
uiresume;

function varargout = prev10(h, eventdata, handles, varargin)
info = guidata(h);
if info.trlop > 10,
  info.trlop = info.trlop - 10;
else
  info.trlop = 1;
end;
guidata(h,info);
uiresume;

function varargout = next10(h, eventdata, handles, varargin)
info = guidata(h);
if info.trlop < info.ntrl - 10,
  info.trlop = info.trlop + 10;
else
  info.trlop = info.ntrl;
end;
guidata(h,info);
uiresume;

function edittext(h,eventdata)
info = guidata(h);
user_string = get(h,'String');
user_number = str2double(user_string);                                     clear user_string
if user_number > info.ntrl
    info.trlop = info.ntrl;
    guidata(h,info);
    uiresume;
elseif isempty(user_number)
    errordlg('trial # must be a number!')
elseif isnan(user_number)
    errordlg('trial # must be a number!')
else
    info.trlop = user_number;
    guidata(h,info);
    uiresume;
end

function varargout = markgood_next(varargin)
markgood(varargin{:});
next(varargin{:});

function varargout = markblink_next(varargin)
markblink(varargin{:});
next(varargin{:});

function varargout = markmove_next(varargin)
markmove(varargin{:});
next(varargin{:});

function varargout = marktongue_next(varargin)
marktongue(varargin{:});
next(varargin{:});

function varargout = markmuscle_next(varargin)
markmuscle(varargin{:});
next(varargin{:});

function varargout = markelse_next(varargin)
markelse(varargin{:});
next(varargin{:});

function varargout = markgood_same(varargin)
markgood(varargin{:});
next(varargin{:});
prev(varargin{:});

function varargout = markblink_same(varargin)
markblink(varargin{:});
next(varargin{:});
prev(varargin{:});

function varargout = markmove_same(varargin)
markmove(varargin{:});
next(varargin{:});
prev(varargin{:});

function varargout = marktongue_same(varargin)
marktongue(varargin{:});
next(varargin{:});
prev(varargin{:});

function varargout = markmuscle_same(varargin)
markmuscle(varargin{:});
next(varargin{:});
prev(varargin{:});

function varargout = markelse_same(varargin)
markelse(varargin{:});
next(varargin{:});
prev(varargin{:});

function varargout = labels(h, eventdata, handles, varargin)
info = guidata(h);
if strcmp(info.showlabel,'on')
    info.showlabel = 'off';
elseif strcmp(info.showlabel,'off')
    info.showlabel = 'on';
end
guidata(h,info);
next(h);
prev(h);

function varargout = markgood(h, eventdata, handles, varargin)
info = guidata(h);
info.ArtTab(info.trlop,:) = [1 0 0 0 0 0];
fprintf(description_trial(info));
title(description_trial(info)); 
guidata(h,info);

function varargout = markblink(h, eventdata, handles, varargin)
info = guidata(h);
info.ArtTab(info.trlop,1) = false;
info.ArtTab(info.trlop,2) = true;
fprintf(description_trial(info));
title(description_trial(info)); 
guidata(h,info);

function varargout = markmove(h, eventdata, handles, varargin)
info = guidata(h);
info.ArtTab(info.trlop,1) = false;
info.ArtTab(info.trlop,3) = true;
fprintf(description_trial(info));
title(description_trial(info)); 
guidata(h,info);

function varargout = marktongue(h, eventdata, handles, varargin)
info = guidata(h);
info.ArtTab(info.trlop,1) = false;
info.ArtTab(info.trlop,4) = true;
fprintf(description_trial(info));
title(description_trial(info)); 
guidata(h,info);

function varargout = markmuscle(h, eventdata, handles, varargin)
info = guidata(h);
info.ArtTab(info.trlop,1) = false;
info.ArtTab(info.trlop,5) = true;
fprintf(description_trial(info));
title(description_trial(info)); 
guidata(h,info);

function varargout = markelse(h, eventdata, handles, varargin)
info = guidata(h);
info.ArtTab(info.trlop,1) = false;
info.ArtTab(info.trlop,6) = true;
fprintf(description_trial(info));
title(description_trial(info)); 
guidata(h,info);

function varargout = comment_callback(h, eventdata, handles, varargin)
info = guidata(h);
info.ArtComment{info.trlop,1} = get(h,'String');
fprintf(description_trial(info));
title(description_trial(info)); 
guidata(h,info);

function varargout = stop(h, eventdata, handles, varargin)
info = guidata(h);
info.quit = 1;
guidata(h,info);
uiresume;

function str = description_trial(info)
tmp = ' marked as: ';
ind = find(info.ArtTab(info.trlop,:)==1);
if ~isempty(ind)
    for k = 1:length(ind)
        tmp = [tmp info.ArtLabels{ind(k)} ' '];
    end; clear k
end
str  = sprintf(['trial ' num2str(info.trlop) tmp ' \n'], info.trlop); clear tmp

function redraw(h)
if ~ishandle(h)
  return
end
info = guidata(h);
cla;
title('');
drawnow
hold on
dat  = info.data.trial{info.trlop};
time = info.data.time{info.trlop};
if isfield(info.data,'trial_bli')
    dat_bli = info.data.trial_bli{info.trlop};
end
if isfield(info.data,'trial_mov')
    dat_mov = info.data.trial_mov{info.trlop};
end
if ~isempty(info.cfg.alim)
  % use fixed amplitude limits for amplitude scaling
  amax = info.cfg.alim;
else
  % use automatic amplitude limits for scaling, these are different for each trial
  amax = max(max(abs(dat(info.chansel,:))));
end
tmin = time(1);
tmax = time(end);
% scale the time values between 0.1 and 0.9
time = 0.1 + 0.8*(time-tmin)/(tmax-tmin);
% scale the amplitude values between -0.5 and 0.5, offset should not be removed
dat = dat ./ (2*amax);
if exist('dat_bli','var')
    dat_bli = dat_bli ./ (2*amax);
end
if exist('dat_mov','var')
    dat_mov = dat_mov ./ (2*amax);
end
for row=1:info.nrows
  for col=1:info.ncols
    chanindx = (row-1)*info.ncols + col;
    % scale the time values for this subplot
    tim = (col-1)/info.ncols + time/info.ncols;
    % scale the amplitude values for this subplot
    amp = dat(chanindx,:)*info.fact./info.nrows + 1 - row/(info.nrows+1);
    hold on; plot(tim, amp, 'k')
    if exist('dat_bli','var')
        amp_bli = dat_bli(chanindx,:)./info.nrows + 1 - row/(info.nrows+1);
        hold on; plot(tim, amp_bli, 'r')
    end
    if exist('dat_mov','var')
        amp_mov = dat_mov(chanindx,:)*info.fact./info.nrows + 1 - row/(info.nrows+1);
        hold on; plot(tim, amp_mov, 'g')
    end
    hold on; plot(tim, amp, 'k')
  end
end
if strcmp(info.showlabel,'on')
    hold on; text(info.x, info.y, info.label);
end
uicontrol(h,'units','normalized','position',[.20 .94 .60 .02],'Style','Text','String',description_trial(info));
axis([0 1 0 1.005])
clear amp amp_bli amp_mov
hold off

%% subfunctions
function offset = time2offset_fieldtrip(time, fsample);

% TIME2OFFSET converts a time-axis of a trial into the offset in samples
% according to the definition from DEFINETRIAL
%
% Use as
%   [offset] = time2offset(time, fsample)
%
% The trialdefinition "trl" is an Nx3 matrix. The first column contains
% the sample-indices of the begin of the trial relative to the begin
% of the raw data , the second column contains the sample_indices of
% the end of the trials, and the third column contains the offset of
% the trigger with respect to the trial. An offset of 0 means that
% the first sample of the trial corresponds to the trigger. A positive
% offset indicates that the first sample is later than the triger, a
% negative offset indicates a trial beginning before the trigger.

% Copyright (C) 2005, Robert Oostenveld
%
% Subversion does not use the Log keyword, use 'svn log <filename>' or 'svn -v log | less' to get detailled information

offset = round(time(1)*fsample);

%% subfunctions (2)
function [i] = nearest_fieldtrip(array, val)

% NEAREST return the index of an array nearest to a scalar
% 
% [indx] = nearest(array, val)

% Copyright (C) 2002, Robert Oostenveld
%
% Subversion does not use the Log keyword, use 'svn log <filename>' or 'svn -v log | less' to get detailled information

mbreal(array);
mbreal(val);

mbvector(array);
mbscalar(val);

% ensure that it is a column vector
array = array(:);

if isnan(val)
  error('incorrect value')
end

if val>max(array)
  % return the last occurence of the nearest number
  [dum, i] = max(flipud(array));
  i = length(array) + 1 - i;
else
  % return the first occurence of the nearest number
  [mindist, i] = min(abs(array(:) - val));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mbreal(a)
if ~isreal(a)
  error('Argument to mbreal must be real');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mbscalar(a)
if ~all(size(a)==1)
  error('Argument to mbscalar must be scalar');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SUBFUNCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mbvector(a)
if ndims(a) > 2 | (size(a, 1) > 1 & size(a, 2) > 1)
  error('Argument to mbvector must be a vector');
end

