function signal_ps = THG_phase_scrambling_151203(signal)
%
% signal_ps = THG_phase_scrambling_151203(signal)

%% fft

Y = fft(signal);

%% even number of data points in signal
if mod(length(signal),2) == 0
    
    % number of data points
    n = length(signal)/2;

    % shuffle phases
    for j = 1:n-1

            % amplitudes
            amp1 = abs(Y(j+1));
            amp2 = abs(Y(end-j+1));

            if ~(amp1 == amp2)
                error('inconsistency in matching up symmetry')
            end

            % generate random phase
            phase = rand(1)*2*pi - pi;

            % translate amplitude and phase into complex number
            Z = amp1 * exp(i * phase);

            % phase shuffled fft
            fft_ps(j+1)     = real(Z) + imag(Z)*i;
            fft_ps(n*2-j+1) = real(Z) - imag(Z)*i;

            % clear variables
            clear amp* phase Z

    end; clear j

    % keep first and "middle" frequency
    fft_ps(1)   = Y(1);
    fft_ps(n+1) = Y(n+1);

    % generate new signal
    signal_ps = ifft(fft_ps);

    % % check fft
    % Y_ps = fft(signal_ps);
    % figure; plot(abs(Y)); hold on; plot(abs(Y_ps),'.r')
    % 
    % % plot signal and phase shuffled signal
    % figure; plot(signal)
    % hold on; plot(signal_ps,'r')

%% odd number of data points in signal
elseif mod(length(signal),2) == 1

    % number of data points
    n = (length(signal)-1)/2;

    % shuffle phases
    for j = 1:n

            % amplitudes
            amp1 = abs(Y(j+1));
            amp2 = abs(Y(end-j+1));

            if ~(amp1 == amp2)
                error('inconsistency in matching up symmetry')
            end

            % generate random phase
            phase = rand(1)*2*pi - pi;

            % translate amplitude and phase into complex number
            Z = amp1 * exp(i * phase);

            % phase shuffled fft
            fft_ps(j+1)     = real(Z) + imag(Z)*i;
            fft_ps(n*2-j+2) = real(Z) - imag(Z)*i;

            % clear variables
            clear amp* phase Z

    end; clear j

    % keep first frequency
    fft_ps(1)   = Y(1);

    % generate new signal
    signal_ps = ifft(fft_ps);

    % % check fft
    % Y_ps = fft(signal_ps);
    % fig1 = figure; plot(abs(Y)); hold on; plot(abs(Y_ps),'.r')
    % 
    % % plot signal and phase shuffled signal
    % figure; plot(signal)
    % hold on; plot(signal_ps,'r')

end



