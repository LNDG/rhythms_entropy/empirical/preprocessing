%% define root path

pn.root = '//mpib07/EEG-Desktop2/'; 
% pn.root = '/home/mwb/smb4k/MPIB07/EEG-Desktop2/';
%pn.root = 'M:/';

%% "set path" for analyses

% EEG tools folder
pn.fun = [pn.root 'ComMemEEGTools/'];

% change directory
cd([pn.fun 'fnct_THG'])

% add function folders folders for analysis
THG_addpath_fnct_common(pn.fun,0)
THG_addpath_fnct_thg(pn.fun)
THG_addpath_fieldtrip(pn.fun)

addpath([pn.fun '/fnct_MWB/MWB_ArtfDetec/'],'-end');
addpath([pn.fun '/fnct_MWB/MWB_ArtfDetec/external/'],'-end');

%% define IDs for preprocessing

% general path
pn.gen = [pn.root 'MERLIN/D_data/'];

cd(pn.gen)

files = dir;
cnt = 1;
for j = 1:length(files)
    if length(files(j).name) == 4 && files(j).isdir == 1
        ID(cnt,1) = str2num(files(j).name);
        cnt = cnt + 1;
    end
end; clear j cnt

%% define experimental BLOCKs for preprocessing

BLOCK = {'L','R1'};

%% loop IDs & BLOCKS

cnt_id = 1;

for id = 1:30
for b  = 2

    id
    
    try
        
%%  define folders

    % path: history (e.g., EEG marked segments)
    pn.hst = [pn.gen '/' num2str(ID(id)) '/history/'];

    % path: raw EEG data 
    pn.beh = [pn.gen '/' num2str(ID(id)) '/beh/'];

    % path: raw EEG data 
    pn.dat = [pn.gen '/' num2str(ID(id)) '/raw_eeg/'];

    % path: processed eeg data
    pn.eeg = [pn.gen '/' num2str(ID(id)) '/eeg/'];


% function THG__20140312(pn,ID,BLOCK,segment)

% pn = path structure requiring following fields: 
% .hst = history folder
% .beh = raw behavioral data folder
% .eeg = raw eeg data folder
% .out = processed behavioral data folder
%
% ID has to be numeric

% required sub-functions:
% - 

%% read config file
load([pn.hst num2str(ID(id)) '_config_' BLOCK{b} '.mat'])

%% read eeg data
load([pn.eeg num2str(ID(id)) '_'  BLOCK{b} '_Rlm_Fhl_Ica_Seg_Art.mat'])

%% read behavioral data
load([pn.beh 'Merlin_' num2str(ID(id)) '.mat'])

%% define data segments / conditions

% eeg trials
trl = config.ArtDect.final2original;

for j = 1:size(trl,1)
    
    % get index
    for k = 1:size(indData,1)
        %if indData{k,15}==trl(j,2)
        %    ind = k;
        end
    end; clear k
    
    % get memory performance
    trl(j,4) = indData{ind,4};
    
    % clear variables
    %clear ind
    
end; clear j

%% collect data (1)

% cnt1 = 1;
% cnt2 = 1;
% 
% for k = 1:length(data.trial)
%     
%     k
% 
%     % hilbert transform
%     tmp = data.trial{1};
%     
%     if trl(k,4) == 1
%         
%         dat1(cnt1,:,:) = tmp;
%         cnt1 = cnt1 + 1;
%         
%     elseif trl(k,4) == 0
%         
%         dat2(cnt2,:,:) = tmp;
%         cnt2 = cnt2 + 1;
%         
%     end
%     
%     % clear variables
%     clear tmp
%     data.trial(1) = [];
%     
% end; clear k
% 
% clear data

%% collect data (2)

dat1 = zeros([sum(trl(:,4)==1) size(data.trial{1})]);
dat2 = zeros([sum(trl(:,4)==0) size(data.trial{1})]);

cnt1 = 1;
cnt2 = 1;

for k = 1:length(data.trial)
    
    if trl(k,4) == 1
        
        dat1(cnt1,:,:) = data.trial{k};
        cnt1 = cnt1 + 1;
        
    elseif trl(k,4) == 0
        
        dat2(cnt2,:,:) = data.trial{k};
        cnt2 = cnt2 + 1;
        
    end
        
end; clear k

clear data

N(cnt_id,1) = size(dat1,1);
N(cnt_id,2) = size(dat2,1);

% collect data
data1(cnt_id,:,:) = mean(dat1);
data2(cnt_id,:,:) = mean(dat2);

% set counter
cnt_id = cnt_id + 1;

% clear variables
clear cnt1 cnt2 config dat1 dat2 indData trl

    end

end; clear b
end; clear id

% time vector
time = [-1000:4:4500];

figure;
boxplot(N)
hold on; plot(1,N(:,1),'og')
hold on; plot(2,N(:,2),'og')

figure;
subplot(211); imagesc(time,[1:60],squeeze(mean(data1)))
subplot(212); imagesc(time,[1:60],squeeze(mean(data2)))

% t-test
[h p ci stats] = ttest(data1,data2);
figure; imagesc(time,[1:60],squeeze(stats.tstat.*h),[-6 6])
figure; imagesc(time,[1:60],squeeze(stats.tstat),[-6 6])

% channels
chan = 30;
figure;  plot(time,squeeze(mean(data1(:,chan,:))))
hold on; plot(time,squeeze(mean(data2(:,chan,:))),'r')
legend('remembered','forgotten')

% channels - errorbars
% chan = 48;
% figure;  errorbar(time,squeeze(mean(data1(:,chan,:))),squeeze(std(data1(:,chan,:))))
% hold on; errorbar(time,squeeze(mean(data2(:,chan,:))),squeeze(std(data2(:,chan,:))),'r')

% % t-test on raw dat
% [h p ci stats] = ttest2(dat1,dat2);
% 
% figure; imagesc(squeeze(stats.tstat.*h))
% 
% figure; imagesc(squeeze(mean(dat1)),[-12 12])
% figure; imagesc(squeeze(mean(dat2)),[-12 12])
% 
% 
% chan = 12;
% figure;  plot(time,squeeze(mean(dat1(:,chan,:))))
% hold on; plot(time,squeeze(mean(dat2(:,chan,:))),'r')
% 
% chan = 48;
% figure;  plot(time,squeeze(mean(dat1(:,chan,:))))
% hold on; plot(time,squeeze(mean(dat2(:,chan,:))),'r')
% 
% 

%%
% for k = 1:length(data.trial)
%     
%     dat(k,:,:) = data.trial{k};
% 
% end; clear k
% 
% time = [-1000:4:4500];
% figure; imagesc(time,[1:60],squeeze(mean(dat)))
% 
% chan = 53;
% figure; plot(time,squeeze(mean(dat(:,chan,:))))
% figure; imagesc(time,[1:size(dat,1)],squeeze(dat(:,chan,:)),[-50 50])
