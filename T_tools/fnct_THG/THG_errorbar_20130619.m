function THG_errorbar_20130619(pnt,avg,sem,width,col,linwidth)

if length(pnt) == 1
    
    hold on; plot([pnt pnt],[avg-sem avg+sem],'color',col,'linewidth',linwidth)
    hold on; plot([pnt-width pnt+width],[avg+sem avg+sem],'color',col,'linewidth',linwidth)
    hold on; plot([pnt-width pnt+width],[avg-sem avg-sem],'color',col,'linewidth',linwidth)

elseif length(pnt) > 1
    
    for j = 1:length(pnt)
    
        hold on; plot([pnt(j) pnt(j)],[avg(j)-sem(j) avg(j)+sem(j)],'color',col,'linewidth',linwidth)
        hold on; plot([pnt(j)-width pnt(j)+width],[avg(j)+sem(j) avg(j)+sem(j)],'color',col,'linewidth',linwidth)
        hold on; plot([pnt(j)-width pnt(j)+width],[avg(j)-sem(j) avg(j)-sem(j)],'color',col,'linewidth',linwidth)

    end
    
end