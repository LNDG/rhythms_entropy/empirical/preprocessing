function Z = THG_fisher_Z_130619(r)

for i = 1:size(r,1)
for j = 1:size(r,2)
for k = 1:size(r,3)
    if r(i,j,k) < abs (.999)
        Z(i,j,k) = 1/2*log((1+r(i,j,k))./(1-r(i,j,k)));
    elseif r(i,j,k) >= abs (.999)
        r(i,j,k) = .999;
        Z(i,j,k) = 1/2*log((1+r(i,j,k))./(1-r(i,j,k)));
    elseif isnan(r(i,j,k))
        Z(i,j,k) = NaN;
    end
end; clear k
end; clear j
end; clear i
