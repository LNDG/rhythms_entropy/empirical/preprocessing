function [z p] = THG_Steiger_Z1star_130627(ABC,type)
%
% testing whether the correlations r(A,B) = r(A,C)
% see Bortz (1999), p. 213
%
% ABC = [n x 3]; n = sample size
% first column should contain common variable to both correlations

corr(ABC,'type',type)

% get correlation coefficients
r_ab = corr(ABC(:,1),ABC(:,2),'type',type);
r_ac = corr(ABC(:,1),ABC(:,3),'type',type);
r_bc = corr(ABC(:,2),ABC(:,3),'type',type);

% Fisher Z transformed correlation coefficients
Zab = THG_fisher_Z_130619(r_ab);
Zac = THG_fisher_Z_130619(r_ac);
Zbc = THG_fisher_Z_130619(r_bc);

% sample size
n = size(ABC,1);

% r_a.
r_a_ = (r_ab + r_ac) / 2;

CV1 = ( 1 ./ ( (1 - r_a_.^2) .^ 2 ) ) * ...
      ( (r_bc .* (1 - 2*r_a_.^2)) - ...
        (1/2 .* r_a_.^2 * (1 - 2.*r_a_.^2 - r_bc.^2) ) );

% calculate z-value
z = ( sqrt(n - 3) * (Zab - Zac) ) / sqrt(2 - 2.*CV1);

% p-value
[h p ci zval] = ztest(z,0,1);