function [Pepisode,detected,B] = THG_BOSC_standard_wrapper_20150603(data,cfg)
%
% [Pepisode,detected,B] = THG_BOSC_standard_wrapper_20150603(data,cfg)
%
% wrapper for BOSC
%
% input: data = [n x 1] time series
%        cfg  = config structure specifying BOSC parameters:
%               - .F          = frequency vector
%               - .fsample    = sampling frequency
%               - .wavenumber = number of cycles in Morlet wavelet
%               - .padding    = time before and after the actual time series 
%                               of interest [in sec]
%               - .percentile = BOSC percentile threshold
%               - .numcycles  = number of cycles required to qualify as
%                               oscillation
% output: Pepisode [vector]
%         detected [matrix]
%         B        [matrix]

% 03.06.2015 THG


    % wavelet transform
    B = BOSC_tf(data,cfg.F,cfg.fsample,cfg.wavenumber);
    
    % B matrix w/o padding
    B = B(:,cfg.padding*cfg.fsample+1:end-cfg.padding*cfg.fsample);
    
    % background power estimation
    [pv,mp] = BOSC_bgfit(cfg.F,B); % [pv,mp] = BOSC_bgfit_THG_20150614(cfg.F,B)
    
    % BOSC thresholds
    [pt,dt] = BOSC_thresholds(cfg.fsample,cfg.percentile,cfg.numcycles,cfg.F,mp);
    
    % oscillation detection
    detected = zeros(size(B));
    for f = 1:length(cfg.F)
        detected(f,:) = BOSC_detect(B(f,:),pt(f),dt(f),cfg.fsample);
    end; clear f

    % Pepisode
    Pepisode = mean(detected,2);
    
