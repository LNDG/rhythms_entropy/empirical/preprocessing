%% define root path

pn.root = '//mpib07/EEG-Desktop2/'; % '/home/mwb/smb4k/MPIB07/EEG-Desktop2/'

%% "set path" for analyses

% EEG tools folder
pn.fun = [pn.root 'ComMemEEGTools/'];

% change directory
cd([pn.fun 'fnct_THG'])

% add function folders folders for analysis
THG_addpath_fnct_common(pn.fun,0)
THG_addpath_fieldtrip(pn.fun)

%% read & merge trigger

ID    = 3102;
BLOCK = 'L'; % BLOCK = 'R1'

%% construct paths

% path: general
pn.gen = '//mpib07/EEG-Desktop2/MERLIN/D_data/';

% path: history (e.g., EEG marked segments)
pn.hst = [pn.gen num2str(ID) '/history/'];

% path: raw behavioral data
pn.beh = [pn.gen num2str(ID) '/raw_beh/'];

% path: raw eeg data
pn.eeg = [pn.gen num2str(ID) '/raw_eeg/'];

% path: processed behavioral data
pn.out = [pn.gen num2str(ID) '/beh/'];

THG_read_and_merge_trigger_MERLIN_20140302(pn,ID,BLOCK)