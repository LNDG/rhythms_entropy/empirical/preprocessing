function data = SS_switchChannels_Study_noA1(data)

    % 170915 | JQK created function
    % 180109 | JQK altered for STSW study
    %        | channels should have been recorded correctly, only renaming to fit ConMem scheme
    
    % INPUT: data | fieldtrip data preprocessing structure
    
    % Use for data, where A1 and FCz have NOT been exchanged.

    alteredChans = {'HEOGL' 'LHEOG';'HEOGR' 'RHEOG';'VEOG' 'IOR'; 'REF', 'A2'};

    % change special channels to what we want them to be
    for indChan = 1:size(alteredChans,1)
        channelRecorded = alteredChans{indChan,1};
        channelIntended = alteredChans(indChan,2);
        data.label(strcmp(data.label,channelRecorded)) = channelIntended;
    end
    
end