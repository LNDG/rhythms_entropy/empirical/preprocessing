function data = SS_switchChannels_Study(data)

    % 170915 | JQK created function
    % 180109 | JQK altered for STSW study
    %        | channels should have been recorded correctly, only renaming to fit ConMem scheme
    
    % INPUT: data | fieldtrip data preprocessing structure
    
    % TP9 (A1) and TP10 (appear to have been exchanged for most recordings)

    alteredChans = {'HEOGL' 'LHEOG';'HEOGR' 'RHEOG';'VEOG' 'IOR'; 'A1' 'FCz'; 'FCz' 'A1'; 'REF' 'A2'};

    % change special channels to what we want them to be
    for indChan = 1:size(alteredChans,1)
        channelRecorded = alteredChans{indChan,1};
        channelIntended = alteredChans(indChan,2);
        switchChannel(indChan) = find(strcmp(data.label,channelRecorded));
    end
    for indChan = 1:size(alteredChans,1)
        data.label(switchChannel(indChan)) = alteredChans(indChan,2);
    end
    
end