function data = SS_switchChannels_Pilot(data)

    % 170915 | JQK created function
    
    % INPUT: data | fieldtrip data preprocessing structure

    green = {'Fp1';'Fp2';'F7';'F3';'Fz';'F4';'F8';'FC5';'FC1';'FC2';'FC6';'T7';'C3';'Cz';'C4';'T8';'TP9';'CP5';'CP1';'CP2';'CP6';'TP10';'P7';'P3';'Pz';'P4';'P8';'PO9';'O1';'Oz';'O2';'PO10'};
    yellow = {'AF7';'AF3';'AF4';'AF8';'F5';'F1';'F2';'F6';'FT9';'FT7';'FC3';'FC4';'FT8';'FT10';'C5';'C1';'C2';'C6';'TP7';'CP3';'CPz';'CP4';'TP8';'P5';'P1';'P2';'P6';'PO7';'PO3';'POz';'PO4';'PO8'};

    alteredChans = {'REF' 'REF' 'REF' 'A2';'TP10' 'A1' 'CP4' 'A1';'TP9' 'FCz' 'C2' 'FCz';'FT9' 'HEOGL' 'FC1' 'LHEOG';'FT10' 'HEOGR' 'Cz' 'RHEOG'; 'P1' 'VEOG' 'Pz' 'IOR'};

    % Note that TP9 and TP10 were changed during the pilot!
    
    % change 'special channels' to what they WERE recorded as (recording label)
    for indChan = 1:size(alteredChans,1)
        channelRecorded = alteredChans{indChan,2};
        channelCorrected = alteredChans(indChan,1);
        data.label(strcmp(data.label,channelRecorded)) = channelCorrected;
    end
    
    % change all channels to what they should have been recorded as (setup label)
    for indChan = 1:size(green,1)
        channel1 = green(indChan,1);
        channel2 = yellow(indChan,1);
        idx1 = find(strcmp(data.label,channel1));
        idx2 = find(strcmp(data.label,channel2));
        data.label(idx1) = channel2;
        data.label(idx2) = channel1;
    end

    % change special channels to what we want them to be
    for indChan = 1:size(alteredChans,1)
        channelRecorded = alteredChans{indChan,1};
        channelIntended = alteredChans(indChan,4);
        data.label(strcmp(data.label,channelRecorded)) = channelIntended;
    end
    
end