%% Import STSWD resting EEG data into EEGlab

% 180321 | adapted from STSWD Stroop

%% initialize

clear; close all; pack; clc;

%% load in path & toolboxes

pn.study    = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root = [pn.study, 'dynamic/data/eeg/rest/A_preproc/SA_preproc_study/'];
pn.eeg_IN   = [pn.eeg_root, 'B_data/A_raw/'];
pn.data_OUT = [pn.eeg_root, 'B_data/B_EEG_eeglab/']; mkdir(pn.data_OUT);
pn.logs     = [pn.eeg_root, 'Y_logs/A_EEG_eeglab_logs/']; mkdir(pn.logs);
pn.eeglab   = [pn.eeg_root, 'T_tools/eeglab14_1_1b/']; addpath(pn.eeglab);
pn.fnct_JQK = [pn.eeg_root, 'T_tools/fnct_JQK/']; addpath(genpath(pn.fnct_JQK));

%% open eeglab

eeglab % use eegh to get history of EEGlab commands!

%% define IDs for preprocessing

% N = 48 YA, 52 OAs
% 2201 - no rest available

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1213';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%% merge EEG & ET data

for indID = 1:length(IDs)
    % Define ID
    ID = IDs{indID};

    condEEG = ['rest'];

    % create log
    diary([pn.logs, num2str(ID) ,'_', condEEG , '_ET_EEG_notes_',date,'.txt']);

    %% load raw EEG data by run

    eegIn = ['ss_',num2str(ID), '_',condEEG,'.vhdr'];
    mrkIn = ['ss_',num2str(ID), '_',condEEG,'.vmrk'];
    dataOut_eeg = [pn.data_OUT, num2str(ID), '_', condEEG, '_EEG'];
    dataOut_mrk = [pn.data_OUT, num2str(ID), '_', condEEG, '_mrk_EEG.mat'];
    %try
        % load markers
        mrk = bva_readmarker([pn.eeg_IN, mrkIn]);
        % if multiple marker files are available: concatenate them
        if iscell(mrk)
            mrk = cat(2,mrk{:});
        end
        % find run on/offset
        onsets = find(mrk(1,:)== 1); % first onset marker
        if isempty(onsets)
            disp(['Onset missing: ', ID, '!']);
            onsets = 1;
        end;
        offsets = find(mrk(1,:)== 8); % final offset marker [set to 128]
        if isempty(offsets)
            disp(['Offset missing: ', ID, '!']);
            offsets = size(mrk,2);
        end;
        % check whether any on/offset markers are incorrect (e.g. restarted presentation)
        excludeOnsets = find(diff(onsets)<1000);
        excludeOffsets = find(diff(offsets)<1000);
        if ~isempty(excludeOnsets)
            onsets(excludeOnsets) = [];
        end
        if ~isempty(excludeOffsets)
            offsets(excludeOffsets) = [];
        end
        tmp_onsetMrk = mrk(:,onsets);
        tmp_offsetMrk = mrk(:,offsets);
        SegmentMat = [tmp_onsetMrk(2,:)', tmp_offsetMrk(2,:)'];
        fprintf(['Run length is: ', num2str(diff(SegmentMat, [], 2)'), '\n']);
        % load data
        EEG = pop_loadbv(pn.eeg_IN, eegIn, SegmentMat);
        [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'setname','data_EEG','savenew',dataOut_eeg,'gui','off');
        % save mrk/event files
        event = EEG.event;
        urevent = EEG.urevent;
        save(dataOut_mrk, 'event', 'urevent')
        clear EEG;
%     catch
%         disp(['There has been an error: ', ID, '!']);
%     end
    diary off
end % ID
