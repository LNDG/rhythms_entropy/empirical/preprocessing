%% D_ICA_labeling_resting_20141217

% The central idea here is to take the full data and label the ICA based on
% the data across runs. The labeled components will then be allocated to
% the individual runs, such that ICA exclusion can be performed on the run
% data.

% required functions:
% - cm_label_ica_gui_20180116

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.study        = '/Volumes/LNDG/Projects/StateSwitch/';
pn.eeg_root     = [pn.study, 'dynamic/data/eeg/rest/A_preproc/SA_preproc_study/'];
pn.EEG          = [pn.eeg_root, 'B_data/C_EEG_FT/'];
pn.History      = [pn.eeg_root, 'B_data/D_History/'];
% add ConMemEEG tools
pn.MWBtools     = [pn.eeg_root, 'T_tools/fnct_MWB/'];           addpath(genpath(pn.MWBtools));
pn.THGtools     = [pn.eeg_root, 'T_tools/fnct_THG/'];           addpath(genpath(pn.THGtools));
pn.commontools  = [pn.eeg_root, 'T_tools/fnct_common/'];        addpath(genpath(pn.commontools));
pn.fnct_JQK     = [pn.eeg_root, 'T_tools/fnct_JQK/'];           addpath(genpath(pn.fnct_JQK));
pn.FT           = [pn.eeg_root, 'T_tools/fieldtrip-20170904/']; addpath(pn.FT); ft_defaults;

%% available IDs

% N = 47 YA, 52 OAs
% 2201 - no rest available; 1213 dropped (weird channel arrangement)

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

condEEG = ['rest'];

%% SELECT ID for visual screening

ID = 2261;

%% load data & start GUI

% load data (full data)
load([pn.EEG, num2str(ID), '_', condEEG, '_EEG_Rlm_Fhl_Ica.mat'],'data')

if ~isfield(data.iclabels,'emg')
    data.iclabels.emg = [];
end

% load config (ICA version)
load([pn.History, num2str(ID), '_', condEEG, '_config.mat'],'config')

% settings for ICA

% ica config
cfg.method           = 'runica';
%cfg.channel          = {'all','-ECG','-A2'};
cfg.trials           = 'all';
cfg.numcomponent     = 'all';
cfg.demean           = 'no';
cfg.runica.extended  = 1;

% ICA solution for segments
cfg.unmixing     = data.unmixing;
cfg.topolabel    = data.topolabel;

% ICA (from weights)

% components
comp = ft_componentanalysis(cfg,data);
    
% include electrode information
comp.elec = data.elec;

% include ICA labeling
comp.iclabels = data.iclabels;

% clear cfg
clear cfg

% label ICs

% settings
cfg.topoall  = 'yes';
cfg.chanlocs = data.chanlocs;

% label electrodes
data.iclabels = cm_label_ica_gui_20180116(cfg,comp);

% keep labels in config
config.ica1.iclabels.manual = data.iclabels;

% clear variables
clear cfg

% save data

% save data (full data)
save([pn.EEG, num2str(ID), '_', condEEG, '_EEG_Rlm_Fhl_Ica.mat'],'data')

% save config (ICA version)
save([pn.History, num2str(ID), '_', condEEG, '_config.mat'],'config')

% clear variables

clear BLOCK ID comp config data
