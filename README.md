These scripts perform the preprocessing for the resting state EEG data described in Kosciessa et al. (2019, bioRxiv).
--

##### B_STSW_ImportEEG_rest
-	Import EEG data

##### C_STSW_prepare_preprocessing_rest
-	Prepare for ICA
-	Read into FieldTrip format
-	Switch channels (see note below)
-	EEG settings:
    -	Referenced to avg. mastoid (A1, A2)
    -	downsample: 1000Hz to 250 Hz
    -	4th order Butterworth 1-100 Hz BPF
    -	no reref for ECG

##### D_STSW_visual_inspection_for_ica_rest
-	Visual labeling of gross noise periods that should not be considered for ICA

##### D5_InspectChannelArrangement_v2
-	Check whether the correlational structure of the channels looks sensible. If not, there may have been problems with electrode placement.

##### E_STSW_ica1_rest
-	Conduct initial ICA1

##### F_STSW_ICA_labeling
-	Manual labeling of artefactual ICA components

##### G_STSW_segmentation_raw_data
-	Segmentation: XXX
-	Load raw data
-	Switch channels
-	EEG settings: 
    -	Referenced to avg. mastoid (A1, A2)
    -	0.2 4th order butterworth HPF
    -	125 4th order butterworth LPF
    -	demean
    -	recover implicit reference: A2
    -	downsample: 1000Hz to 500 Hz

##### H_STSW_automatic_artifact_correction
-	Automatic artifact correction, interpolation
-	Remove blink, move, heart, ref, art & emg ICA components prior to calculation
-	get artifact contaminated channels by kurtosis, low & high frequency artifacts
-	get artifact contaminated channels by FASTER
-	interpolate artifact contaminated channels
-	equalize duration of trials to the trial with fewest samples
-	get artifact contaminated epochs & exclude epochs recursively
-	get channel x epoch artifacts
-	Note that this does NOT yet remove anything. We only calculate the data to be removed in the next step (I).

##### I_STSW_prep_data_for_analysis
-	Remove blink, move, heart, ref, art & emg ICA components
-	Interpolate detected artifact channels
-	Remove artifact-heavy trials, for subjects with missing onsets, the missing trials are included here as ‘artefactual trials’, hence correcting the EEG-behavior assignment:

Note that the channels A1 and FCz were generally exchanged in the BrainVisionRecorder. During some sessions, it was also apparent that the amplifiers had been switched. These channels have been switched based on visual inspection of the expected autocorrelation of neighboring channels. Note that this switching is only done AFTER the data are read into FieldTrip! The scripts that do the switching can be found in the directory A_scripts/helper/. C_STSW_prepare_preprocessing_rest contains the conditionals for what is switched in which subject. Visual inspection of ICAs indicated that the channel switch was successful, although outlier channels may still be slightly off.